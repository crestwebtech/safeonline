<?php

namespace App\Models;

use Eloquent, Request;

class Language extends Eloquent{
	
	protected $table = 'tbl_language';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
        'v_key',
		'v_title',
		'v_image',
		'i_order', 
		'e_status',
    ];
	
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
	public static function getAdminDropDownArray(){
		$_data = array();
		$_temp = self::orderBy('i_order')->get();
		if( $_temp->count() ){
			foreach( $_temp as $row ){
				$_data[ $row->v_key ] = $row->v_title;
			}
		}
        return $_data;
    }
	
	public static function getFrontArray(){
		$_data = array();
		$_temp = self::where("e_status", "=", "active")->orderBy('i_order')->get();
		if( $_temp->count() ){
			foreach( $_temp as $row ){
				
				$row->v_image = _uploads( $row->v_image, 'language' );
				
				$_data[ $row->v_key ] = $row;
			}
		}
        return $_data;
    }
	
	public static function getDropDownArray(){
		$_data = array();
		$_temp = self::where( "e_status", "=", "active" )->orderBy('i_order')->get();
		if( $_temp->count() ){
			foreach( $_temp as $row ){
				$_data[ $row->v_key ] = $row->v_title;
			}
		}
        return $_data;
    }
	
}

