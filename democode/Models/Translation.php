<?php

namespace App\Models;

use Eloquent, Request, Storage;

class Translation extends Eloquent{
	
	protected $table = 'tbl_translation';
	
	public $timestamps = false;
	
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
        'v_key',
		'v_title',
    ];
	
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
	
	public static function langData( $v_key ){
		
		$_lang_arr 	= _get_admin_langs();
		$_lang_data = array();
		foreach( $_lang_arr as $_lang => $_lang_title ){
			$_lang_data[$_lang] = '';
			$fileName = $_lang.'.json';
			if( Storage::disk('language-translation')->exists( $fileName ) ){
				$fileData = Storage::disk('language-translation')->get( $fileName );
				if( $fileData ){
					$fileData = json_decode( $fileData, true );
					$_lang_data[$_lang] = isset( $fileData[$v_key] ) ? $fileData[$v_key] : '';
				}
			}
		}
		
		return $_lang_data;
		
    }
	
	
	
	
}

