<?php

namespace App\Models;

use Eloquent, Request, Storage;

class CronTracker extends Eloquent{
	
	protected $table = 'tbl_cron_tracker';
	
	public $timestamps = false;
	
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
        'v_type',
		'v_last_id',
    ];
	
	public static function getTracker( $v_type ){
		$obj = self::query();
		$row = $obj->where( 'v_type', '=', $v_type )->first();
		if( count( $row ) ){
			return $row;
		}
		else{
			return self::create( array(
				'v_type' => $v_type,
				'v_last_id' => 0,
			) );
		}
    }
	
}

