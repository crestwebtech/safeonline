<?php

namespace App\Models;


use Eloquent, Request;

class AssetsResults extends Eloquent{
	
	protected $table = 'tbl_assets_result';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
	protected $fillable = [
        'i_user_id',
		'v_user_id',
		'v_result_id',
		'v_result_category',
		'v_result_title',
		'v_result_source',
		'v_result_risk',
		'f_result_percent',
		'v_result',
		'd_scan_date',
		'e_status',
    ];
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
}