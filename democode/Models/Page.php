<?php

namespace App\Models;


use Eloquent, Request;

class Page extends Eloquent{
	
	protected $table = 'tbl_page';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
	protected $fillable = [
        'v_slug', 
		'v_title', 
		'e_status',
    ];
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
	public function langFront(){
		$_lang = _lang();
		$rowData = $this->hasMany('App\Models\PageLang', 'i_row_id', 'id' )->where('v_lang', '=', $_lang );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			$getData[0]->v_image = _uploads( $getData[0]->v_image, 'homepage-slider' );
			return $getData[0]; // ->toArray()
		}
		return $returnArr;
    }
	
	public function langData(){
		$rowData = $this->hasMany('App\Models\PageLang', 'i_row_id', 'id' );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			foreach( $getData as $row ){
				$returnArr[ $row->v_lang ] = $row->toArray();
			}	
		}
		return $returnArr;
    }
	
	public static function front(){
		$returnArr = array();
		$_data = self::query();
		$rowData = $_data->where( "e_status", "=", "active" )->orderBy('i_order')->get();
		if( $rowData->count() ){
			foreach( $rowData as $row ){
				$returnArr[$row->id] = $row; // ->toArray()
				$returnArr[$row->id]['lang'] = $row->langFront();
			}
		}
		return $returnArr;
    }
	
}