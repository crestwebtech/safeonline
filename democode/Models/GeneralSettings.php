<?php
namespace App\Models;

// use Illuminate\Foundation\Auth\User as Authenticatable;

use Eloquent, Request;

class GeneralSettings extends Eloquent{
	
	protected $table = 'tbl_sitesettings';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'v_key', 'l_value'
    ];
	
	public $timestamps = false;
	
	
}

