<?php

namespace App\Models;

use Eloquent, Request;

class EmailLang extends Eloquent{
	
	protected $table = 'tbl_email_lang';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
		'i_row_id', 
		'v_lang', 
        'v_subject', 
		'l_description',
    ];
	
}

