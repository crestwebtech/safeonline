<?php

namespace App\Models;


use Eloquent, Request;

class Glossary extends Eloquent{
	
	protected $table = 'tbl_glossary';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
	protected $fillable = [
        'v_char', 
		'v_title', 
		'i_order',
		'e_status',
    ];
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
	public function langData(){
		$rowData = $this->hasMany('App\Models\GlossaryLang', 'i_row_id', 'id' );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			foreach( $getData as $row ){
				$returnArr[ $row->v_lang ] = $row->toArray();
			}	
		}
		return $returnArr;
    }
	
	public static function front( $processed = 0 ){
		$returnArr = array();
		$processedArr = array();
		
		$_data = self::query();
		$rowData = $_data->where( "e_status", "=", "active" )->orderBy('v_char')->orderBy('i_order')->get();
		
		if( $rowData->count() ){
			foreach( $rowData as $row ){
				
				$landData = $row->langFront();
				
				$returnArr[$row->id] = $row; // ->toArray()
				$returnArr[$row->id]['lang'] = $landData;
				
				if( $processed ){
					$processedArr[$row->v_char][] = array(
						'v_title' => $landData->v_title, 
						'l_description' => $landData->l_description,
					);
				}
				
			}
		}
		if( $processed ){
			return $processedArr;
		}
		return $returnArr;
    }
	
	public function langFront(){
		$_lang = _lang();
		$rowData = $this->hasMany('App\Models\GlossaryLang', 'i_row_id', 'id' )->where('v_lang', '=', $_lang );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			return $getData[0];
		}
		return $returnArr;
    }
	
}