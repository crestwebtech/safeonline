<?php

namespace App\Models;

use Eloquent, Request;

class TipsLang extends Eloquent{
	
	protected $table = 'tbl_tips_lang';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
		'i_row_id', 
		'v_lang', 
        'v_title', 
		'l_description'
    ];
	
	
}

