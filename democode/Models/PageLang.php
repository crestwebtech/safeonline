<?php

namespace App\Models;


use Eloquent, Request;

class PageLang extends Eloquent{
	
	protected $table = 'tbl_page_lang';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
		'i_row_id', 
		'v_lang', 
        'v_title', 
		'l_description',
		'v_title_2', 
		'l_description_2',
    ];
	
}