<?php

namespace App\Models;

use Eloquent, Request;

class HomepageSliderLang extends Eloquent{
	
	protected $table = 'tbl_homepage_slider_lang';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
		'i_row_id', 
		'v_lang', 
        'v_title', 
		'v_image', 
		'l_description',
		'v_url',
    ];
	
}

