<?php

namespace App\Models;


use Eloquent, Request;

class FaqType extends Eloquent{
	
	protected $table = 'tbl_faq_type';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
	protected $fillable = [
        'v_title', 
		'i_order', 
		'e_status',
    ];
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
	public function langData(){
		$rowData = $this->hasMany('App\Models\FaqTypeLang', 'i_row_id', 'id' );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			foreach( $getData as $row ){
				$returnArr[ $row->v_lang ] = $row->toArray();
			}	
		}
		return $returnArr;
    }
	
	public function langFront(){
		$_lang = _lang();
		$rowData = $this->hasMany('App\Models\FaqTypeLang', 'i_row_id', 'id' )->where('v_lang', '=', $_lang );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			// $getData[0]->v_image = _uploads( $getData[0]->v_image, 'homepage-slider' );
			return $getData[0]; // ->toArray()
		}
		return $returnArr;
    }
	
	public static function front(){
		$returnArr = array();
		$_data = self::query();
		$rowData = $_data->where( "e_status", "=", "active" )->orderBy('i_order')->get();
		if( $rowData->count() ){
			foreach( $rowData as $row ){
				$returnArr[$row->id] = $row; // ->toArray()
				$returnArr[$row->id]['lang'] = $row->langFront();
			}
		}
		return $returnArr;
    }
	
	
	public static function getFrontList(){
		$_data = self::query()->where( "e_status", "=", "active" )->orderBy('i_order')->get();
		return $_data;
    }
	
	public function currLang(){
		return $this->hasOne('App\Models\FaqTypeLang', 'i_row_id', 'id' )->where( "v_lang", "=", _lang() );
    }
	
	public function hasChildFAQs(){
		return $this->hasMany('App\Models\Faq', 'i_faq_type_id', 'id' )
			->where( "e_status", "=", "active" )
			->where( "i_parent_id", "=", "0" )->orderBy('i_order');
    }
	
}