<?php

namespace App\Models;


use Eloquent, Request;

class TipsType extends Eloquent{
	
	protected $table = 'tbl_tips_type';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
	protected $fillable = [
        'v_slug', 
		'v_title', 
		'v_icon', 
		'i_order', 
		'e_status',
    ];
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
	public function langData(){
		$rowData = $this->hasMany('App\Models\TipsTypeLang', 'i_row_id', 'id' );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			foreach( $getData as $row ){
				$returnArr[ $row->v_lang ] = $row->toArray();
			}	
		}
		return $returnArr;
    }
	
	public function langFront(){
		$_lang = _lang();
		$rowData = $this->hasMany('App\Models\TipsTypeLang', 'i_row_id', 'id' )->where('v_lang', '=', $_lang );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			// $getData[0]->v_image = _uploads( $getData[0]->v_image, 'homepage-slider' );
			return $getData[0]; // ->toArray()
		}
		return $returnArr;
    }
	
	public static function front(){
		$returnArr = array();
		$_data = self::query();
		$rowData = $_data->where( "e_status", "=", "active" )->orderBy('i_order')->get();
		if( $rowData->count() ){
			foreach( $rowData as $row ){
				$returnArr[$row->id] = $row; // ->toArray()
				$returnArr[$row->id]['lang'] = $row->langFront();
			}
		}
		return $returnArr;
    }
	
	
	public static function getFrontList(){
		$_data = self::query()->where( "e_status", "=", "active" )->orderBy('i_order')->get();
		return $_data;
    }
	
	public function currLang(){
		return $this->hasOne('App\Models\TipsTypeLang', 'i_row_id', 'id' )->where( "v_lang", "=", _lang() );
    }
	
	public function frontChildTipsGroup(){
		return $this->hasMany('App\Models\TipsGroup', 'i_tips_type_id', 'id' )->where( 'e_status', '=', 'active' )->orderBy( 'i_order', 'ASC' );
    }
	
	
	
	
}