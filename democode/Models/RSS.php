<?php

namespace App\Models;

use Eloquent, Request;

class RSS extends Eloquent{
	
	protected $table = 'tbl_rss_feed';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
        'v_source_url',
		'v_id',
		'v_image',
		'v_title',
		'l_description',
		'v_url',
		'v_author',
		'd_added',
		'e_status',
    ];
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
	public static function getFrontList(){
		$_data = self::query()->where( "e_status", "=", "active" )->orderBy( "d_added", "DESC" )->get();
		return $_data;
    }
	
}

