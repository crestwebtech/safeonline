<?php

namespace App\Models;

use Eloquent, Request;

class CountryLang extends Eloquent{
	
	protected $table = 'tbl_country_lang';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
		'i_row_id', 
		'v_lang', 
        'v_title', 
    ];
	
}

