<?php

namespace App\Models;

use Eloquent, Request;

class TipsTypeLang extends Eloquent{
	
	protected $table = 'tbl_tips_type_lang';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
		'i_row_id', 
		'v_lang', 
        'v_title', 
		'v_title_2', 
		'l_description', 
    ];
	
}

