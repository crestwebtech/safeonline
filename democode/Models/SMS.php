<?php

namespace App\Models;

use Eloquent, Request;

class SMS extends Eloquent{
	
	protected $table = 'tbl_sms';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
        'v_key', 
		'v_subject', 
		'e_status',
    ];
	
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
	public static function getFront( $key ){
		
        $_data = self::query();
		
		$row = $_data->where( 'v_key', '=', $key )->get()->first();
		$row['lang'] = $row->langFront()->toArray();
		
		$row = $row->toArray();
		
		return $row;
		
    }
	
	public function langData(){
		$rowData = $this->hasMany('App\Models\SMSLang', 'i_row_id', 'id' );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			foreach( $getData as $row ){
				$returnArr[ $row->v_lang ] = $row->toArray();
			}	
		}
		return $returnArr;
    }
	
	public function langFront(){
		$returnArr = array();
		$rowData = $this->hasMany('App\Models\SMSLang', 'i_row_id', 'id' )->where( 'v_lang', '=', _lang() );
		if( $rowData->count() ){
			$getData = $rowData->get();
			return $getData[0];
		}
		return $returnArr;
    }
	
	
	
	
}

