<?php

namespace App\Models;

use Eloquent, Request;

class FaqLang extends Eloquent{
	
	protected $table = 'tbl_faq_lang';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
		'i_row_id', 
		'v_lang', 
        'v_title', 
		'l_description'
    ];
	
	
}

