<?php

namespace App\Models;

use Eloquent, Request;

class Country extends Eloquent{
	
	protected $table = 'tbl_country';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
        'v_title',
		'v_code',
		'e_status',
    ];
	
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		return _d_paginate( $obj, $params );
    }
	
	public function langData(){
		$rowData = $this->hasMany('App\Models\CountryLang', 'i_row_id', 'id' );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			foreach( $getData as $row ){
				$returnArr[ $row->v_lang ] = $row->toArray();
			}	
		}
		return $returnArr;
    }
	
	public function currLang(){
		return $this->hasOne('App\Models\CountryLang', 'i_row_id', 'id' )->where( "v_lang", "=", _lang() );
    }
	
	public static function getFrontList(){
		$_data = self::query()->where( "e_status", "=", "active" )->orderBy('v_title')->get();
		return $_data;
    }
	
	
	
}

