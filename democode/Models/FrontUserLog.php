<?php

namespace App\Models;

use Eloquent, Request, Storage;

class FrontUserLog extends Eloquent{
	
	protected $table = 'tbl_users_log';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
        'i_user_id',
		'v_ip',
		'login_at',
		'logout_at',
    ];
	
	
}

