<?php

namespace App\Models;


use Eloquent, Request;

class TipsGroup extends Eloquent{
	
	protected $table = 'tbl_tips_group';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
	protected $fillable = [
        'i_tips_type_id',
		'v_title',
		'i_order',
		'e_status',
    ];
	
	public static function ApplySearch( $params = array() ){
		$obj = self::query();
		$obj->selectRaw( 
			'tbl_tips_group.*,
			tips_type.id AS tips_type_id,
			tips_type.v_title AS tips_type_title'
		);
		$obj->leftJoin( 'tbl_tips_type AS tips_type', 'tips_type.id', '=', 'tbl_tips_group.i_tips_type_id' );
		
		return _d_paginate( $obj, $params );
    }
	
	public function langData(){
		$rowData = $this->hasMany('App\Models\TipsGroupLang', 'i_row_id', 'id' );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			foreach( $getData as $row ){
				$returnArr[ $row->v_lang ] = $row->toArray();
			}	
		}
		return $returnArr;
    }
	
	public function langFront(){
		$_lang = _lang();
		$rowData = $this->hasMany('App\Models\TipsGroupLang', 'i_row_id', 'id' )->where('v_lang', '=', $_lang );
		$returnArr = array();
		if( $rowData->count() ){
			$getData = $rowData->get();
			// $getData[0]->v_image = _uploads( $getData[0]->v_image, 'homepage-slider' );
			return $getData[0]; // ->toArray()
		}
		return $returnArr;
    }
	
	public static function front(){
		$returnArr = array();
		$_data = self::query();
		$rowData = $_data->where( "e_status", "=", "active" )->orderBy('i_order')->get();
		if( $rowData->count() ){
			foreach( $rowData as $row ){
				$returnArr[$row->id] = $row; // ->toArray()
				$returnArr[$row->id]['lang'] = $row->langFront();
			}
		}
		return $returnArr;
    }
	
	public function currLang(){
		return $this->hasOne('App\Models\TipsGroupLang', 'i_row_id', 'id' )->where( "v_lang", "=", _lang() );
    }
	
	public function frontChildTips(){
		return $this->hasMany('App\Models\Tips', 'i_tips_group_id', 'id' );
    }
	
}