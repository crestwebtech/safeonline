<?php

namespace App\Models;

use Eloquent, Request;

class SMSLang extends Eloquent{
	
	protected $table = 'tbl_sms_lang';
	
	public $timestamps = false;
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	
    protected $fillable = [
		'i_row_id', 
		'v_lang', 
        'l_description',
    ];
	
}

