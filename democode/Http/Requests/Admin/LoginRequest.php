<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;


class LoginRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	
	public function authorize(){
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	
	public function rules(){
		return [
			'v_email' => 'required|email|min:4',
			'password' => 'required|min:5',
			
		];
	}
}
