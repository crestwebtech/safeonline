<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;


class RegisterRequest extends Request {



	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	
	public function authorize(){
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	
	public function rules(){
		return [
			'v_first_name' 	=> 'required|max:255',
			'v_email' 		=> 'required|email|min:4|unique:tbl_users',
			'password' 		=> 'required|min:5|confirmed',
		];
	}
}
