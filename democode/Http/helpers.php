<?php
use App\Models\GeneralSettings,
	App\Models\Language,
	App\Models\Country,
	App\Models\Email,
	App\Models\SMS,

	App\LanguageTranslation;
	
use Session AS Session;
use Storage AS Storage;
use SimpleXMLElement AS SimpleXMLElement;


use PHPMailerAutoload AS PHPMailerAutoload; 
use PHPMailer AS PHPMailer;

	
	function _p( $data ){ 
		echo '<pre>'.print_r( $data, true ).'</pre>';
	}
	
	function _live(){ 
		if( $_SERVER['HTTP_HOST'] == '192.168.0.222' ) return 0;
		return 1;
	}
	
	
	function _admin_url(){
		return _get_setting('SITE_URL').'admin/';
	}
	function _url( $url ){
		return _get_setting('SITE_URL').$url;
	}
	
	function _get_settings(){
		$temp = GeneralSettings::all();
		$data = array();
		if( $temp->count() ){
			foreach( $temp as $row ){
				$data[ $row->v_key ] = $row->l_value;
			}
		}
		return $data;
	}
	
	function _get_setting( $key = '' ){
		global $_glob_settings;
		if( isset( $_glob_settings[ $key ] ) ){
			return $val = $_glob_settings[ $key ];
		}
		else{
			$temp = GeneralSettings::where( 'v_key', '=', $key )->get();
			return $val = ( $temp->count() && isset( $temp[0]->l_value ) ) ? $temp[0]->l_value : "";
		}
	}
	
	function _get_lang_setting( $key = '' ){
		global $_glob_settings;
		$key = $key.'_'._lang();
		if( isset( $_glob_settings[ $key ] ) ){
			return $val = $_glob_settings[ $key ];
		}
		else{
			$temp = GeneralSettings::where( 'v_key', '=', $key )->get();
			return $val = ( $temp->count() && isset( $temp[0]->l_value ) ) ? $temp[0]->l_value : "";
		}
	}
	
	function _get_admin_langs(){
		return Language::getAdminDropDownArray();
	}
	
	function _langMenu(){
		return Language::getFrontArray();
	}
	
	function _all_translactions( $lang = '' ){
		$lang = $lang ? $lang : Session::get('lang');
		$fileData = Storage::disk('language-translation')->get( $lang.'.json' );
		if( $fileData ){
			$fileData = json_decode( $fileData, true );
		}
		return $fileData;
	}
	
	function _get_lbl( $key = '' ){
		$data = Session::get('translactions');
		return isset( $data[$key] ) ? nl2br( $data[$key] ) : "";
	}
	
	function _lang(){
		return Session::get('lang');
	}
	
	function _admin_sections( $key = '', $subkey = '' ){
		
		$data = array(
			
			'__LOGIN' => array( '_key' => 'login', '_title' => 'Login', '_view' => 'login' ),
			'__LOGOUT' => array( '_key' => 'logout', '_title' => 'Logout', '_view' => 'logout' ),
			'__REGISTER' => array( '_key' => 'register', '_title' => 'Register', '_view' => 'register' ),
			'__RESET_PASSWORD' => array( '_key' => 'password/reset', '_title' => 'Reset Password' ),
			'__GENERAL_SETTINGS' => array( 
				'_key' => 'general-settings', 
				'_title' => 'General Settings', 
				'_view' => 'general-settings' 
			),
			'__TCS_SETTINGS' => array( 
				'_key' => 'tcs-settings', 
				'_title' => 'TCS Settings', 
				'_view' => 'tcs-settings' 
			),
			
			
			'__CUSTOM_HTML_FIELDS' => array( '_key' => 'custom-html-fields', '_title' => 'Custom HTML Fields', '_view' => 'custom-html-fields' ),
			
			'__MANAGE_ADMIN' => array(
				'_key' 	 	=> 'manage-admin',
				'_title' 	=> 'Manage Admin',
				'_single' 	=> 'Admin',
				'_view'  	=> 'admin',
			),
			
			'__MANAGE_CC_USERS' => array(
				'_key' 	 	=> 'manage-ccusers',
				'_title' 	=> 'Manage Call Center Agents',
				'_single' 	=> 'Call Center Agent',
				'_view'  	=> 'manage-ccusers',
			),
			
			
			'__MANAGE_LANGUAGE' => array(
				'_key' 	 	=> 'languages',
				'_title' 	=> 'Manage Languages',
				'_single' 	=> 'Homepage Language',
				'_view'  	=> 'languages',
			),
			
			'__MANAGE_HOMEPAGE_SLIDER' 	=> array(
				'_key' 	 	=> 'homepage-slider',
				'_title' 	=> 'Manage Homepage Slider',
				'_single' 	=> 'Homepage Slider',
				'_view'  	=> 'homepage-slider',
			),
			'__MANAGE_TRANSLATION' 	=> array(
				'_key' 	 	=> 'manage-translation',
				'_title' 	=> 'Manage Translation',
				'_single' 	=> 'Manage Translation',
				'_view'  	=> 'manage-translation',
			),
			'__MANAGE_COUNTRY' 	=> array(
				'_key' 	 	=> 'country',
				'_title' 	=> 'Manage Country',
				'_single' 	=> 'Manage Country',
				'_view'  	=> 'country',
			),
			
			
			
			'__MANAGE_EMAIL_SETTINGS' => array(
				'_key' 	 	=> 'email-settings',
				'_title' 	=> 'Email Settings',
				'_single' 	=> 'Email Settings',
				'_view'  	=> 'email-settings',
			),
			
			'__MANAGE_EMAIL' => array(
				'_key' 	 	=> 'email',
				'_title' 	=> 'Manage Emails',
				'_single' 	=> 'Email',
				'_view'  	=> 'email',
			),
			
			'__MANAGE_SMS_SETTINGS' => array(
				'_key' 	 	=> 'sms-settings',
				'_title' 	=> 'SMS Settings',
				'_single' 	=> 'SMS Settings',
				'_view'  	=> 'sms-settings',
			),
			
			'__MANAGE_SMS' => array(
				'_key' 	 	=> 'sms',
				'_title' 	=> 'Manage SMS',
				'_single' 	=> 'SMS',
				'_view'  	=> 'sms',
			),
			
			'__MANAGE_FAQ_TYPES' => array(
				'_key' 	 	=> 'faq-types',
				'_title' 	=> 'Manage FAQ Types',
				'_single' 	=> 'FAQ Type',
				'_view'  	=> 'faq-types',
			),
			
			'__MANAGE_FAQS' => array(
				'_key' 	 	=> 'faqs',
				'_title' 	=> 'Manage FAQs',
				'_single' 	=> 'FAQ',
				'_view'  	=> 'faqs',
			),
			
			'__MANAGE_GLOSSARY' => array(
				'_key' 	 	=> 'glossary',
				'_title' 	=> 'Manage Glossaries',
				'_single' 	=> 'Glossary',
				'_view'  	=> 'glossary',
			),
			'__MANAGE_TIPS_TYPES' => array(
				'_key' 	 	=> 'tips-types',
				'_title' 	=> 'Manage Tips & Tricks Types',
				'_single' 	=> 'Tips Type',
				'_view'  	=> 'tips-types',
			),
			'__MANAGE_TIPS_GROUP' => array(
				'_key' 	 	=> 'tips-groups',
				'_title' 	=> 'Manage Tips & Tricks Groups',
				'_single' 	=> 'Tips Group',
				'_view'  	=> 'tips-groups',
			),
			'__MANAGE_TIPS' => array(
				'_key' 	 	=> 'tips',
				'_title' 	=> 'Manage Tips & Tricks',
				'_single' 	=> 'Tip',
				'_view'  	=> 'tips',
			),
			'__MANAGE_PAGES' => array(
				'_key' 	 	=> 'pages',
				'_title' 	=> 'Manage Pages',
				'_single' 	=> 'Page',
				'_view'  	=> 'page',
			),
			
			'__MANAGE_RSS_FEEDS' => array(
				'_key' 	 	=> 'rss-feeds',
				'_title' 	=> 'Manage RSS Feeds',
				'_single' 	=> 'RSS Feeds',
				'_view'  	=> 'rss-feeds',
			),
			
			'__MANAGE_USERS' 	=> array(
				'_key' 	 	=> 'users',
				'_title' 	=> 'Manage Users',
				'_single' 	=> 'Manage User',
				'_view'  	=> 'users',
			),
			'__MANAGE_USERS_ASSETS_RESULTS' => array(
				'_key' 	 	=> 'users-assets-results',
				'_title' 	=> 'Manage Assets Results',
				'_single' 	=> 'Assets Result',
				'_view'  	=> 'users-assets-results',
			),
		);
		
		$preFixRoute = 'admin/';
		
		if( _admin_user( 'cc_admin' ) ){
			$preFixRoute = 'cc_admin/';
		}
		
		foreach( $data as $k => $v ){
			$v['_key'] 		= $preFixRoute.( isset( $v['_key'] ) ? $v['_key'] 	: '' );
			$v['_view'] 	= 'admin/'.( isset( $v['_view'] ) ? $v['_view'] 	: '' );
			$v['_title'] 	= isset( $v['_title'] ) && $v['_title'] ? $v['_title'] 	: '';
			$v['_single'] 	= isset( $v['_single'] ) && $v['_single'] ? $v['_single'] : '';
			$v['_list'] 	= 'List of Records';
			$v['_add'] 		= 'Add Record';
			$v['_edit'] 	= 'Edit Record';
			$data[$k] 		= $v;
		}
		
		foreach( $data as $k => $v ){
			$data[$k]['_url'] = url( $v['_key'] );
			$data[$k]['_active'] = Request::is( $v['_key'] ) ? "active" : "";
			if( $data[$k]['_add'] && Request::is( $v['_key'].'/*' ) ){ 
				$data[$k]['_active'] = "active"; 
			}
			if( $data[$k]['_edit'] && Request::is( $v['_key'].'/*' ) ){ 
				$data[$k]['_active'] = "active"; 
			}
		}
		$return = isset( $data[$key] ) ? $data[$key] : $data;
		if( $key && $subkey ){
			return $data[$key][$subkey];
		}
		return $return;
	}
	function _admin_left_sidebar(){
		
		$arr = array(
			'__GENERAL_SETTINGS' => array(
				'_childs' => array(
					'__GENERAL_SETTINGS',
					'__TCS_SETTINGS',
					'__CUSTOM_HTML_FIELDS',
					'__MANAGE_LANGUAGE',
					'__MANAGE_HOMEPAGE_SLIDER',
					'__MANAGE_TRANSLATION',
					'__MANAGE_COUNTRY'
				)
			),
			
			'__MANAGE_ADMIN' => array(
				'_title' => 'Manage Staff',
				'_childs' => array( '__MANAGE_ADMIN', '__MANAGE_CC_USERS' )
			),
			
			
			'__MANAGE_EMAIL_SETTINGS' => array( '_childs' => array( '__MANAGE_EMAIL_SETTINGS', '__MANAGE_EMAIL' ) ),
			'__MANAGE_SMS_SETTINGS' => array( '_childs' => array( '__MANAGE_SMS_SETTINGS', '__MANAGE_SMS' ) ),
			
			'__MANAGE_USERS' => array( '_childs' => array( '__MANAGE_USERS', '__MANAGE_USERS_ASSETS_RESULTS' ) ),
			
			'__MANAGE_FAQS' => array( '_childs' => array( '__MANAGE_FAQ_TYPES', '__MANAGE_FAQS' ) ),
			'__MANAGE_TIPS' => array( '_childs' => array( '__MANAGE_TIPS_TYPES', '__MANAGE_TIPS_GROUP', '__MANAGE_TIPS' ) ),
			'__MANAGE_GLOSSARY' => array(),
			
			'__MANAGE_PAGES' => array(),
			
			'__MANAGE_RSS_FEEDS' => array(),
			
		);
		
		if( _admin_user( 'cc_admin' ) ){
			$arr = array(
				'__MANAGE_USERS' => array(),
				'__MANAGE_USERS_ASSETS_RESULTS' => array(),
			);
		}
		
		return $arr;
		
	}
	
	function _back_url(){
		return URL::previous();
	}
	
	function _get_external_icons(){
		echo '&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://zavoloklom.github.io/material-design-iconic-font/icons.html" target="_blank" >(Material Icons)</a>';
		echo '&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://fontawesome.io/icons/" target="_blank" >(Font Awesome Icons)</a>';
	}
	function _print_html( $str ){
		return str_replace( '<', '&lt;', str_replace( '>', '&gt;', $str ) );
	}
	
	function _put_edit( $section, $row, $show ){
		return '<a href="'.( $section['_url'].'/edit/'.$row->id ).'" >'.$show.'</a>';
	}
	function _put_status( $section, $row, $drop = 0 ){
		if( $row->e_status == 'active' ){
			return '<a href="'.( $section['_url'].'/inactive/'.$row->id ).'" >'.( $drop ? 'Inactive' : ucwords( $row->e_status ) ).'</a>';
		} 
		else { 
			return '<a href="'.( $section['_url'].'/active/'.$row->id ).'" >'.( $drop ? 'Active' : ucwords( $row->e_status ) ).'</a>';
		} 
	}
	
	
	function _admin_user( $role = 'admin' ){
		if( auth()->guard( $role )->check() ){
			return 1;
		}
		return 0;
	}
	
	function _user( $key = '' ){
		
		if( !auth()->guard('frontuser')->check() ){
			return '';
		}
		
		if( $key == 'name' ){
			return auth()->guard('frontuser')->user()->v_first_name." ".auth()->guard('frontuser')->user()->v_surname;
		}
		if( $key == 'country_title' ){
			return auth()->guard('frontuser')->user()->country( 'country_title' );
		}
		if( $key == 'country_code' ){
			return auth()->guard('frontuser')->user()->country( 'v_code' );
		}
		if( $key == 'dob' && $dt = auth()->guard('frontuser')->user()->d_birthdate ){
			if( $dt == '0000-00-00' ) return '';
			return date( 'm/d/Y', strtotime( $dt ) );
		}
		
		if( $key ){
			return auth()->guard('frontuser')->user()->$key;
		}
		else{
			return auth()->guard('frontuser')->user();
		}
	}
	
	
	function _get_alert_msg( $key = '' ){
		$data = array(
			'logout'			=>	'Logout Successfully.',
			'register'			=>	'Register Successfully.',
			'invalid_login'		=>	'Invalid Login.',
			'account_inactive'	=>	'Account Inactive.',
			'added'				=>	'Record added successfully.',
			'updated'			=>	'Record updated successfully.',
			'deleted'			=>	'Record deleted successfully.',
			'not_found'			=>	'Record not found.',
			'exists_email'		=>	'Email already exists.',
			'exists_slug'		=>	'Slug already exists.',
			'exists_key'		=>	'Key already exists.',
			
			'email_invalid'		=>	'Invalid Email Address.',
			'email_sent'		=>	'Email sent successfully.',
			'email_not_sent'	=>	'Email can not sent.',
			
			'sms_invalid'		=>	'Invalid SMS Number.',
			'sms_sent'			=>	'SMS sent successfully.',
			'sms_not_sent'		=>	'SMS can not sent.',
			
			'required_password'	=>	'Password required.',
			
		);
		return isset( $data[ $key ] ) ? $data[ $key ] : "";
	}
	
	function _putval( $arr = array(), $key = '', $defaultVal = '' ){
		return isset( $arr[ $key ] ) ? $arr[ $key ] : $defaultVal;
	}
	
	function _show_alert_msg( $arr = '' ){
		$str = '';
		$msgs = Session::get( 'msg' );
		if( $msgs && gettype( $msgs ) == 'string' ) $msgs = array( $msgs );
		
		if( is_array( $msgs ) && count( $msgs ) ){
			foreach( $msgs as $m ){
				$m = explode( ':', $m );
				$arr = $m; unset( $arr[0] );
				if( $m[0] == 'info0' ){ $m[0] = 0; $m[1] = implode( ':', $arr ); }
				else if( $m[0] == 'info1' ){ $m[0] = 1; $m[1] = implode( ':', $arr ); }
				else{ $m[1] = _get_alert_msg( $m[1] ); }
				
				$str .= '<div class="alert '.( $m[0] ? 'alert-success' : 'alert-danger' ).'">'.$m[1].'</div>';
			}
		}
		Session::set( 'msg', '' );
		return $str;
	}
	
	function _show_front_alert_msg(){
		$str = '';
		$msgs = Session::get( 'msg' );
		if( $msgs && gettype( $msgs ) == 'string' ) $msgs = array( $msgs );
		if( is_array( $msgs ) && count( $msgs ) ){
			foreach( $msgs as $m ){
				$m = explode( ':', $m );
				$str .= '<div class="alert '.( $m[0] ? 'alert-success' : 'alert-danger' ).'">
					'.( _get_lbl( $m[1] ) ? _get_lbl( $m[1] ) : $m[1] ).'
				</div>';
			}
		}
		Session::set( 'msg', '' );
		return $str;
	}
	
	function _seo_text( $str = "" ){
		$str = trim( $str );
		$special_array = array( '#','$','\'','"','?','&',':','!','%','&reg;','&trade;','(',')','/',',','_' );
		$str = str_replace( '&', 'and', $str );
		foreach( $special_array as $item ){ $str = str_replace( $item, ' ', $str ); }
		$str = str_replace( '  ', ' ', trim( $str ) );
		$str = str_replace( '  ', ' ', trim( $str ) );
		$str = str_replace( ' ', '-', trim( $str ) );
		return $str;
	}
	
	##### File Functions #####
	function _makefilename( $str = "" ){
		return time()."-"._seo_text( $str );
	}
	function _filepath( $folder = "other" ){
		return 'public/uploads/'.$folder;
	}
	function _uploads( $file = "", $type = "other" ){
		if( !trim( $file ) ){ return ""; }
		$folder = $type;
		if( $type == "" ){ }
		$file_path = 'public/uploads/'.( $folder ? $folder.'/' : '' ).$file;
		if( file_exists( $file_path ) ){ 
			return url( $file_path );
		}
		return "";
	}
	function _put_list_icon( $file = "", $folder = "other", $padding = 25 ){
		$file_path = 'public/uploads/'.( $folder ? $folder.'/' : '' ).$file;
		if( $file && file_exists( $file_path ) ){ 
			return '<img src="'.url( $file_path ).'" class="admin_uploaded_img" style="max-width:100px;" >';
		}
		return '<span style="padding:'.$padding.'px;display:inline-block;">No Image</span>';
	}
	##### File Functions #####
	
	function _keyval_dropdown( $arr = array(), $selected = '' ){
		$str = '';
		if( count( $arr ) ){
			foreach( $arr as $k => $v ){
				$str .= '<option value="'.$k.'" '.( $k == $selected ? 'selected' : '' ).' >'.ucfirst( $v ).'</option>';
			}
		}
		return $str;
	}
	
	function _keywords(){
		return array(
			"[user_id]" 					=> "User ID",
			"[user_tcs_id]" 				=> "User TCS ID",
			"[user_firstname]" 				=> "User First Name",
			"[user_surname]" 				=> "User Surname",
			"[user_phone]" 					=> "User Phone",
			"[user_email]" 					=> "User Email",
			"[forgot_password_link]" 		=> "Forgot Password Link",
			"[free_text]" 					=> "Free Text",
			"[user_assets_result_table]" 	=> "User Assets Result Table",
			"[result_count]" 				=> "Total Results Count",
			"[password]" 					=> "Password",
			"[result_id]" 					=> "Result ID",
		);
	}
	
	function _dropdown( $arr = array(), $selected = '' ){
		$str = '';
		if( count( $arr ) ){
			foreach( $arr as $v ){
				$str .= '<option value="'.$v.'" '.( $v == $selected ? 'selected' : '' ).' >'.ucfirst( $v ).'</option>';
			}
		}
		return $str;
	}
	
	function _render_table_header( $arr = array() ){
			
			if( !$arr ){ return ''; }
			
			$ascArr = array( 'ASC' => 'DESC', 'DESC' => 'ASC' );
			
			$link = array();
			$key_array = array(
				'keyword', 'page',
			);
			foreach( $_GET as $k => $v ){
				if( in_array( $k, $key_array ) || substr( $k, 0, 5 ) == 'srch_' ){
					$link[] = $k.'='.$v;
				}
			}
			
			$thArr = array();
			foreach( $arr as $k => $v ){
				if( isset( $v['order'] ) && $v['order'] ){
					$sb = _putval( $_GET, 'sb' );
					$st = _putval( $_GET, 'st' );
					$isActive = $sb == $k ? 'active' : '';
					$sortIcon = $isActive ? '&nbsp; '.( $st == 'ASC' ? '<i class="fa fa-long-arrow-up"></i>' : '<i class="fa fa-long-arrow-down"></i>' ) : '';
					$url = $link;
					$url[] = 'sb='.$k;
					$url[] = 'st='.( $sb == $k ? $ascArr[ $st] : 'ASC'  );
					$thArr[] = '<th><a class="'.$isActive.'" href="'.( '?'.implode( '&', $url ) ).'" >'.$v['title'].''.$sortIcon.'</a></th>';
				}
				else if( $k == 'action' ){
					$thArr[] = '<th class="text-right" >'.$v['title'].'</th>';
				}
				else{
					$thArr[] = '<th>'.$v['title'].'</th>';
				}
			}
			
			return '<thead><tr>'.implode( '', $thArr ).'</tr></thead>';
		}
	
	function _d_paginate( $obj, $options = array() ){
		
		$Request = Request::all();
		
		$obj = _putval( $options, 'obj', $obj );
		$type = _putval( $options, 'type' );
		$search = explode( ',', str_replace( ' ', '', _putval( $options, 'search' ) ) );
		$order = explode( ',', str_replace( ' ', '', _putval( $options, 'order' ) ) );
		
        $_GET['keyword'] = $keyword = _putval( $_GET, 'keyword' );
		
		$_GET['sb'] = $sb = trim( _putval( $_GET, 'sb' ) );
		$_GET['st'] = $st = trim( _putval( $_GET, 'st' ) );
		
		$_GET['sb'] = $sb = $sb ? $sb : $order[0];
		$_GET['st'] = $st = $st ? $st : $order[1];
		
		
		if( $keyword ){
			foreach( $search as $k => $s ){
				if( !$k ) $obj->where( $s, 'LIKE', "%".$keyword."%" );
				else $obj->orWhere( $s, 'LIKE', "%".$keyword."%" );
			}
		}
		
		if( $type == 'tips_group' && $srch = _putval( $_GET, 'srch_tips_type' ) ){
			$obj->where( 'i_tips_type_id', '=', $srch );
		}
		else if( $type == 'tips' ){
			if( $srch = _putval( $_GET, 'srch_tips_type' ) ) $obj->where( 'tbl_tips.i_tips_type_id', '=', $srch );
			if( $srch = _putval( $_GET, 'srch_tips_group' ) ) $obj->where( 'tbl_tips.i_tips_group_id', '=', $srch );
		}
		else if( $type == 'assets_results' ){
			if( $srch = _putval( $_GET, 'srch_user_id' ) ) $obj->where( 'i_user_id', '=', $srch );
			if( $srch = _putval( $_GET, 'srch_status' ) ) $obj->where( 'e_status', '=', $srch );
			if( $srch = _putval( $_GET, 'srch_delete' ) ) $obj->where( 'i_delete', '=', $srch );
		}
		else if( $type == 'cc_admin_assets_results' ){
			if( $srch = _putval( $_GET, 'srch_user_id' ) ) $obj->where( 'i_user_id', '=', $srch );
			if( $srch = _putval( $_GET, 'srch_status' ) ) $obj->where( 'e_status', '=', $srch );
			if( $srch = _putval( $_GET, 'srch_delete' ) ) $obj->where( 'i_delete', '=', $srch );
			// $obj->where( 'i_delete', '=', 1 );
		}
		
		
		return $obj->orderBy( $sb, $st )
			->paginate( _get_setting( 'RECORD_PER_PAGE' ) )
			->appends( Request::except( 'page' ) );
		
	}
	
	function _getNormalDate( $date, $format = '' ){
		return date( ( $format ? $format : 'Y-m-d H:i:s' ), $date->sec );
	}
	
	function _date( $date = "", $type = '' ){
		if( !trim( $date ) ) return "";
		if( $type == 'db' ){
			return date( 'Y-m-d', strtotime( $date ) );
		}
		else{
			return date( 'm/d/Y', strtotime( $date ) );
		}
	}
	
	function _set_date( $date = '', $type = 'front' ){
		if( !trim( $date ) ) return '';
		if( $type == 'front' ){
			if( strlen( trim( $date ) ) == 10 ){
				return date( 'm/d/Y', strtotime( $date ) );
			}
			return date( 'm/d/Y h:i A', strtotime( $date ) );
		}
	}
	
	function _set_parent_child_arr( $data_arr = array(), $parentId = 0, $pname = '' ) {
		$return_arr = array();
		foreach( $data_arr as $row ){
			if( $row['parent_id'] == $parentId ){
				$children = _set_parent_child_arr( $data_arr, $row['id'], ( $pname ? $pname." &raquo; " : '' ).$row['title'] );
				if( $children ) {
					$row['child'] = $children;
				}
				$row['pname'] = $pname;
				$return_arr[$row['id']] = $row;
			}
		}
		return $return_arr;
	}
	
	function _get_parent_child_arr( $data = array(), $arr = array() ) {
		if( count( $data ) ){
			foreach( $data as $row ){
				$arr[ $row["id"] ] = ( $row["pname"] ? $row["pname"]." &raquo; " : "" ).$row["title"];
				if( isset( $row["child"] ) ){
					$arr = _get_parent_child_arr( $row["child"], $arr );
				}
			}
		}
		return $arr;
	}
	
	function _parent_child_dropdown( $data = array(), $seleted = '', $str = '' ){
		$str = '';
		if( count( $data ) ){
			foreach( $data as $row ){
				$id = $row["id"];
				$title = $row["title"];
				$parent_id = $row["parent_id"];
				$str .= ( '<option value="'.$id.'" '.( $id == $seleted ? 'selected' : '' ).' >'.( $row["pname"] ? $row["pname"]." &raquo; " : "" ).$title.'</option>' );
				if( isset( $row["child"] ) ){
					$str .= _parent_child_dropdown( $row["child"], $seleted, $str );
				}
			}
		}
		return $str;
	}
	
	function _curl_call( $url, $data = array() ){
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		$contents = curl_exec($ch);
		curl_close( $ch );
		$contentsObj = json_decode( $contents, true );
		return $contentsObj;
	}
	
	function _curl( $url, $fields = array(), $method = 'GET' ){ 
		
		if( $method == 'GET' ){
		   try{
				if( count( $fields ) ){
					$url = $url.'?'.http_build_query( $fields );
				}
				
				// _p( $url );
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url ); 
				curl_setopt($ch, CURLOPT_ENCODING, '');
				curl_setopt($ch, CURLOPT_POST, 0 ); 
				curl_setopt($ch, CURLOPT_FAILONERROR, 1 ); 
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 ); 
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt($ch, CURLOPT_TIMEOUT, 10000 ); 
				$retValue = curl_exec($ch);
				curl_close($ch);
			}
			catch( Exception $e ){
			}
		}
		else{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url ); 
			curl_setopt($ch, CURLOPT_POST, 1 );
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $fields ) );
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false ); 
			curl_setopt($ch, CURLOPT_TIMEOUT, 10000 ); 
			$retValue = curl_exec($ch);
			curl_close($ch);
		}
		return $retValue; 
	}
	
	function _front_contries(){
		$returnArr = array();
		$temp = Country::getFrontList();
		if( $temp->count() ){
			foreach( $temp as $row ){
				$returnArr[$row->id] = $row->currLang->v_title;
			}
		}
		return $returnArr;
	}
		
	function _send_sms( $options = array() ){
		
		$_keywordsAll 	= _keywords();
		
		$_to 			= $options['_to'];
		$_key 			= isset( $options['_key'] ) ? $options['_key'] : '';
		$_keywords 		= isset( $options['_keywords'] ) ? $options['_keywords'] : array();
		$_lang 			= isset( $options['_lang'] ) ? $options['_lang'] : _lang();
		$_body 			= isset( $options['_body'] ) ? $options['_body'] : '';
		
		if( $_key ){
			$SMS 	= SMS::getFront( $_key );
			$_body 	= $SMS['lang']['l_description'];
		}
		
		foreach( $_keywordsAll as $k => $v ){
			$_body = str_replace( $k, ( isset( $_keywords[$k] ) ? $_keywords[$k] : '' ), $_body );
		}
		
		$SMS_URL 		= _get_setting('SMS_URL');
		$SMS_FROM_NAME 	= _get_setting('SMS_FROM_NAME');
		$SMS_APP_ID 	= _get_setting('SMS_APP_ID');
		$SMS_USERNAME 	= _get_setting('SMS_USERNAME');
		$SMS_PASSWORD 	= _get_setting('SMS_PASSWORD');
		
		return $sms = _curl( $SMS_URL, array(
			'from' 		=> $SMS_FROM_NAME,
			'user' 		=> $SMS_USERNAME,
			'password' 	=> $SMS_PASSWORD,
			'api_id' 	=> $SMS_APP_ID,
			'to' 		=> $_to,
			'text' 		=> $_body,
		) );
		
	}
	
	function _send_mail( $options = array() ){
		
		$_to 			= $options['_to'];
		$_to_name 		= isset( $options['_to_name'] ) ? $options['_to_name'] : '';
		$_key 			= isset( $options['_key'] ) ? $options['_key'] : '';
		$_keywords 		= isset( $options['_keywords'] ) ? $options['_keywords'] : array();
		$_lang 			= isset( $options['_lang'] ) ? $options['_lang'] : _lang();
		
		$_keywordsAll 	= _keywords();
		
		$_debug			= isset( $options['_debug'] ) ? $options['_debug'] : 0;
		
		$_subject 		= isset( $options['_subject'] ) ? $options['_subject'] : '';
		$_body 			= isset( $options['_body'] ) ? $options['_body'] : '';
		
		$emailSubject 	= $_subject;
		$emailBody 		= _get_setting( 'EMAIL_TEMPLATE_'.$_lang );
		
		if( $_key ){
			$emailTemplate 	= Email::getFront( $_key );
			$_subject		= $emailTemplate['lang']['v_subject'];
			$_body 			= $emailTemplate['lang']['l_description'];
		}
		
		$emailSubject 			= $_subject;
		$emailBody 				= str_replace( '[email_body]', $_body, $emailBody );
		
		if( $_debug ){
			// _p( 'EMAIL_TEMPLATE_'.$_lang );
			
		}
		
		foreach( $_keywordsAll as $k => $v ){
			$emailSubject = str_replace( $k, ( isset( $_keywords[$k] ) ? $_keywords[$k] : '' ), $emailSubject );
			$emailBody = str_replace( $k, ( isset( $_keywords[$k] ) ? $_keywords[$k] : '' ), $emailBody );
		}
		
		$EMAIL_WITH 			= _get_setting('EMAIL_WITH');
		$EMAIL_FROM_NAME 		= _get_setting('EMAIL_FROM_NAME');
		$EMAIL_FROM_EMAIL 		= _get_setting('EMAIL_FROM_EMAIL');
		$EMAIL_REPLY_NAME 		= _get_setting('EMAIL_REPLY_NAME');
		$EMAIL_REPLY_EMAIL 		= _get_setting('EMAIL_REPLY_EMAIL');
		$EMAIL_SMTP_HOST 		= _get_setting('EMAIL_SMTP_HOST');
		$EMAIL_SMTP_PORT 		= _get_setting('EMAIL_SMTP_PORT');
		$EMAIL_SMTP_USERNAME 	= _get_setting('EMAIL_SMTP_USERNAME');
		$EMAIL_SMTP_PASSWORD 	= _get_setting('EMAIL_SMTP_PASSWORD');
		$EMAIL_SMTP_ENCRYPTION	= _get_setting('EMAIL_SMTP_ENCRYPTION');
		$EMAIL_SMTP_AUTH 		= _get_setting('EMAIL_SMTP_AUTH');
		
		try{
			
			$mail = new PHPMailer;
			$mail->CharSet = "utf-8";
			
			if( $_debug ){
				$mail->SMTPDebug = 2;
			}
			// $mail->SMTPDebug = 2;
			
			$mail->setFrom( $EMAIL_FROM_EMAIL, $EMAIL_FROM_NAME );
			$mail->AddReplyTo( $EMAIL_REPLY_EMAIL, $EMAIL_REPLY_NAME );
			$mail->addAddress( $_to, $_to_name );
			
			$mail->Subject = $emailSubject;
			$mail->MsgHTML( $emailBody );
			
			if( $EMAIL_WITH == 'SMTP' ){
				$mail->isSMTP();
				$mail->Host 		= $EMAIL_SMTP_HOST;
				$mail->Port 		= (int) $EMAIL_SMTP_PORT;
				$mail->SMTPAuth 	= ( $EMAIL_SMTP_AUTH == 'yes' ) ? true : false;
				$mail->SMTPSecure 	= $EMAIL_SMTP_ENCRYPTION;
				$mail->Username 	= $EMAIL_SMTP_USERNAME;
				$mail->Password 	= $EMAIL_SMTP_PASSWORD;
			}
			
			$mail->isHTML( true );
			
			
			
			if( $_debug ){ // && $_to == 'deven.crestinfotech@gmail.com' 
				//$mail->send();
				//_p( $mail ); exit;
				return $mail->send(); 
			}
			// _p($mail->send());
			if( $mail->send() ){
				return 1;
			}
			else{
				return 0;
			}
			
		}
		catch( Exception $e ){
			return $e->getMessage();
			return 0;
			
			
		}
		
	}
	
	function _valid_request( $url ){
		$host_1 = str_replace( 'www.', '', _get_host( $url ) );
		$host_2 = str_replace( 'www.', '', _get_host( _url( '' ) ) );
		if( $host_1 == $host_2 ){
			return 1;
		}
		return 0;
	}
	
	function _get_host( $url ){
		$parsedURL = parse_url( $url );
		if( isset( $parsedURL['host'] ) ){
			return $parsedURL['host'];
		}
		return $url;
	}
	
	function _get_ip(){
			
		if( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ){
			## check ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		else if ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ){
			## to check ip is pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
?>