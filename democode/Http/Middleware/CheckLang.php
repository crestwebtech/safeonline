<?php

namespace App\Http\Middleware;

use Closure, Session;
use Illuminate\Support\Facades\Auth;

use App\FrontUser;

class CheckLang {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
	 
	public function handle( $request, Closure $next ){
		
		## Set Default Lang
		if( !Session::get('lang') ){
			Session::set('lang', _get_setting('SITE_LANGUAGE') );
		}
		
		## Set User Changed Lang
		if( isset( $_REQUEST['lang'] ) && $_REQUEST['lang'] ){
			$temp = _get_admin_langs();
			
			if( isset( $temp[$_REQUEST['lang']] ) ){
				Session::set('lang', $_REQUEST['lang'] );
				
				if( _user( 'id' ) ){
					$row = FrontUser::find( _user( 'id' ) );
					$row->update( array(
						'v_lang' => Session::get('lang')
					) );
				}
				
				
			}
			else{
				Session::set('lang', _get_setting('SITE_LANGUAGE') );
			}
		}
		
		if( _user( 'id' ) ){
			if( !_user( 'v_lang' ) ){
				$row = FrontUser::find( _user( 'id' ) );
				$row->update( array(
					'v_lang' => Session::get('lang')
				) );
			}
		}
		
		## Update Lable Translactions
		Session::set('translactions', _all_translactions() );
		
		return $next( $request );
		// return redirect('/');
	}
	
	/*
    public function handle( $request, Closure $next, $guard = 'admin' ){
		if ( Auth::guard('admin')->check() ) {
	        return $next( $request );
        }
		return redirect('admin')->with([
			'warning' => 'You must have to be logged in.',
		]);
    }*/
}
