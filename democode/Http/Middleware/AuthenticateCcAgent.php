<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateCcAgent {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $guard = 'cc_admin' ){
		if ( Auth::guard('cc_admin')->check() ) {
	        return $next( $request );
        }
		return redirect('cc_admin/login')->with([
			'warning' => 'You must have to be logged in.',
		]);
    }
}
