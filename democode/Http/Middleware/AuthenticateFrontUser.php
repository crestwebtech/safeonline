<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateFrontUser {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $guard = 'frontuser' ){
		if ( Auth::guard('frontuser')->check() ) {
	        return $next( $request );
        }
		return redirect('login')->with([
			'warning' => 'You must have to be logged in.',
		]);
    }
}
