<?php

namespace App\Http\Controllers\Crons;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use Config, DateTime;

use App\Admin;
use App\Models\RSS;


class RssController extends Controller{
	
	public function index( $id ){
		
		$urlArr = array(
			"1" => "https://www.heise.de/security/news/news-atom.xml",
			"2" => "https://www.newsd.admin.ch/newsd/feeds/rss?lang=de&org-nr=1&offer-nr=308",
			//"3" => "https://www.cybercrime.admin.ch/kobik/de/home.rss.html",
		);
		
		$url = $id;
		if( !( $url && isset( $urlArr[ $url ] ) ) ){
			die('Invalid URL.');
		}
		
		if( $url == 1 && !_get_setting('CRON_RSS_1') ){
			die('Cron is disabled by Admin.');
		}
		if( $url == 2 && !_get_setting('CRON_RSS_2') ){
			die('Cron is disabled by Admin.');
		}
		
		$url = $urlArr[ $url ];
		
		$result = file_get_contents( $url );
		
		$xml 	= simplexml_load_string( $result );
		$json 	= json_encode( $xml );
        $array 	= json_decode( $json, TRUE );
		
		if( $url == "https://www.heise.de/security/news/news-atom.xml" ){
			
			//_p( $array ); exit;
			
			if( isset( $array['entry'] ) && count( $array['entry'] ) ){
				foreach( $array['entry'] as $row ){
					
					$date = new DateTime( $row['published'] );
					$ins = array(
						'v_source_url' 	=> $url,
						'v_id' 			=> ( isset( $row['id'] ) ? $row['id'] : '' ),
						'v_image' 		=> ( isset( $row['content']['div']['a']['img']['@attributes']['src'] ) ? $row['content']['div']['a']['img']['@attributes']['src'] : '' ),
						'v_title' 		=> ( isset( $row['title'] ) ? $row['title'] : '' ),
						'l_description' => ( isset( $row['summary'] ) ? $row['summary'] : '' ),
						'v_url' 		=> ( isset( $row['link']['@attributes']['href'] ) ? $row['link']['@attributes']['href'] : '' ),
						'v_author' 		=> ( isset( $array['author']['name'] ) ? $array['author']['name'] : '' ),
						'd_added' 		=> $date->format('Y-m-d H:i:s'),
						'e_status' 		=> 'active',
					);
					
					$rowData = RSS::where( "v_id", "=", $ins['v_id'] )->get();
					if( $rowData->count() ){
						$rowData[0]->update( $ins );
					}
					else{
						RSS::create( $ins );
					}
				}	
			}
		}
		
		else if( $url == "https://www.newsd.admin.ch/newsd/feeds/rss?lang=de&org-nr=1&offer-nr=308" ){
			if( isset( $array['channel']['item'] ) && count( $array['channel']['item'] ) ){
				foreach( $array['channel']['item'] as $row ){
					$ins = array(
						'v_source_url' 	=> $url,
						'v_id' 			=> '',
						'v_image' 		=> '',
						'v_title' 		=> ( isset( $row['title'] ) ? $row['title'] : '' ),
						'l_description' => ( isset( $row['description'] ) ? $row['description'] : '' ),
						'v_url' 		=> ( isset( $row['link'] ) ? $row['link'] : '' ),
						'v_author' 		=> ( isset( $row['author'] ) ? $row['author'] : '' ),
						'd_added' 		=> ( isset( $row['pubDate'] ) ? $row['pubDate'].' 00:00:00' : '' ),
						'e_status' 		=> 'active',
					);
					$rowData = RSS::where( "v_title", "=", $ins['v_title'] )->where( "v_source_url", "=", $url )->get();
					if( $rowData->count() ){
						$rowData[0]->update( $ins );
					}
					else{
						RSS::create( $ins );
					}
				}	
			}
		}
		
		return 'RSS read successfully';
		
	}
	
	public function test(){
		$mail = _send_mail( array(
			'_to' => 'deven.crestinfotech@gmail.com',
			'_to_name' => 'Deven R',
			'_subject' => 'IDProtect Test Cron # '.date('Y-m-d H:i:s'),
			'_body' => 
				'IDProtect Test Cron
				<br>IP : '._get_ip(),
			'_keywords' => array(
			),
		) );
		echo $mail ? 'Sent' : 'Not Sent';
	}
	
	public function sleep1(){
		sleep(10);
		echo 'Sleep 1';
	}
	
	public function sleep2(){
		echo 'Sleep 2';
	}
	
}
