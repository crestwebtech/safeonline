<?php

namespace App\Http\Controllers\Crons;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use Config, DateTime;

use App\FrontUser;
use App\Sentrybay;
use App\Models\CronTracker;
use App\Models\AssetsResults;
use App\Models\GeneralSettings;

class SentrybayController extends Controller{
	
	## Read Extract Results
	public function extractResult(){
		
		if( !_get_setting('CRON_SENTRYBAY_1') ){
			die('Cron is disabled by Admin.');
		}
		
		set_time_limit( 3600 );
		
		if( isset( $_REQUEST['MemberNo'] ) ){
			
			$ExeTimeCountArr = array(
				'start' => microtime(true),
				'end' => 0,
				'exe_time' => 0,
			);
			
			$sentrybayCall = new Sentrybay();
			$responseAsset = $sentrybayCall->apiExtractResult( array(
				"MemberNo" => $_REQUEST['MemberNo'],
			) );
			_p( $responseAsset ); 
			
			$ExeTimeCountArr['end'] = microtime(true);
			$ExeTimeCountArr['exe_time'] = round( ( $ExeTimeCountArr['end'] - $ExeTimeCountArr['start'] ), 3 );
			
			_p( 'Execution Time : ' );
			_p( $ExeTimeCountArr );
			
			exit;
			
		}
		
		
		$lastScan = GeneralSettings::where( 'v_key', '=', 'SENTRYBAY_LAST_SCAN' )->first();
		if( !count( $lastScan ) ){
			GeneralSettings::create( array( 'v_key' => 'SENTRYBAY_LAST_SCAN', 'l_value' => date('Y-m-d H:i:s') ) );
		}
		else{
			$lastScan->l_value = date('Y-m-d H:i:s');
			$lastScan->save();
		}
		
		$v_type = 'sentrybay_extract_result';
		
		$cnt = 0;
		
		while( 1 ){
		
			$_tracker = CronTracker::getTracker( $v_type );
			
			$user = FrontUser::where( 'id', '>', $_tracker->v_last_id )
				->where( 'i_sentrybay', '=', 1 )
				->orderBy( 'id', 'ASC' )
				->first();
			
			if( count( $user ) ){
				
				
				$_translactions = _all_translactions( $user->v_lang );
				
				$sentrybayCall = new Sentrybay();
				$responseAsset = $sentrybayCall->apiExtractResult( array(
					"MemberNo" => $user->v_user_id,
				) );
				
				if( 
					isset( $responseAsset['status_code'] )
					&& $responseAsset['status_code'] == 0
					
					&& isset( $responseAsset['result']['Member']['MemberNo'] )
					&& !is_array( $responseAsset['result']['Member']['MemberNo'] ) 
					&& $responseAsset['result']['Member']['MemberNo'] == $user->v_user_id 
					
					&& isset( $responseAsset['result']['Member']['Result'] )
					&& is_array( $responseAsset['result']['Member']['Result'] ) 
					&& count( $responseAsset['result']['Member']['Result'] )
					){
					
					$user->update( array(
						'v_last_scan' => $responseAsset['result']['Member']['ScanDate'],
					) );
					
						
					$newResults = array();
					
					foreach( $responseAsset['result']['Member']['Result'] as $row ){
						
						$assetsResult = AssetsResults::where( 'v_result_id', '=', $row['Attr']['id'] )
							->where( 'i_user_id', '=', $user->id )
							->first();
						
						$v_result_title = $row['Attr']['category'];
						$v_result_category = explode( '(', $v_result_title );
						$v_result_category = trim( $v_result_category[0] );
						
						$assetsIns = array(
							'i_user_id'			=> $user->id,
							'v_user_id'			=> $user->v_user_id,
							'v_result_id'		=> $row['Attr']['id'],
							'v_result_category'	=> $v_result_category,
							'v_result_title'	=> $v_result_title,
							'v_result_source'	=> $row['Attr']['source'],
							'v_result_risk'		=> $row['Attr']['risk'],
							'f_result_percent'	=> $row['Attr']['percent'],
							'v_result'			=> $row['Value'],
							'd_scan_date'		=> isset( $row['Attr']['date'] ) ? $row['Attr']['date'] : $responseAsset['result']['Member']['ScanDate'],
						);
							
						if( count( $assetsResult ) ){
							$assetsResult->update( $assetsIns );
						}
						else{
							$newResults[] = $assetsIns;
							
							$assetsIns['e_status'] = 'active';
							
							AssetsResults::create( $assetsIns );
						}
						
					}
					
					
					
					
					if( count( $newResults ) ){
						
						$resultsTable = 
						'<table class="result_table" cellspacing="0" cellpadding="10" style="width:100%;border:1px solid #e4e4e4;" border="0" >
							<thead>
								<tr>
									<th align="left" style="border:1px solid #e4e4e4;" >'._putval( $_translactions, 'lbl_risk', 'Risk' ).'</th>
									<th align="left" style="border:1px solid #e4e4e4;" >'._putval( $_translactions, 'lbl_what', 'What?' ).'</th>
									<th align="left" style="border:1px solid #e4e4e4;" >'._putval( $_translactions, 'lbl_where', 'Where?' ).'</th>
									<th align="left" style="border:1px solid #e4e4e4;" >'._putval( $_translactions, 'lbl_link', 'Link' ).'</th>
									<th align="left" style="border:1px solid #e4e4e4;" >'._putval( $_translactions, 'lbl_when', 'When?' ).'</th>
									<th align="left" style="border:1px solid #e4e4e4;" >'._putval( $_translactions, 'lbl_view', 'View' ).'</th>
								</tr>
							</thead>
							<tbody>';
								foreach( $newResults as $row ){
									$colorArr = array(
										'High' => 'red',
										'Medium' => 'orange',
										'Low' => 'green',
									);
									$resultsTable .= 
									'<tr>
										<td style="color:'._putval( $colorArr, $row['v_result_risk'], 'green' ).'; border:1px solid #e4e4e4;">'._putval( $_translactions, 'lbl_sentrybay_risk_'.strtolower( $row['v_result_risk'] ), $row['v_result_risk'] ).'</td>
										<td style="border:1px solid #e4e4e4;" >'.$row['v_result_title'].'</td>
										<td style="border:1px solid #e4e4e4;" >'._putval( $_translactions, 'lbl_sentrybay_source_'.$row['v_result_source'], $row['v_result_source'] ).'</td>
										<td style="border:1px solid #e4e4e4;" >'._get_host( $row['v_result'] ).'</td>
										<td style="border:1px solid #e4e4e4;" >'.$row['d_scan_date'].'</td>
										<td style="border:1px solid #e4e4e4;" ><a href="'.$row['v_result'].'" >'._putval( $_translactions, 'lbl_view', 'View' ).'</a></td>
									</tr>';
								}
								$resultsTable .= 
							'</tbody>
						</table>';
						
						$isEmail = $isSMS = 0;
						if( $user->v_alert == 'both' ){
							$isEmail = $isSMS = 1;
						}
						else if( $user->v_alert == 'email' ){
							$isEmail = 1;
						}
						else if( $user->v_alert == 'sms' ){
							$isSMS = 1;
						}
						
						## Sent Email
						if( $isEmail && $user->v_email ){
							$mail = _send_mail( array(
								'_to' => $user->v_email,
								'_to_name' => $user->v_first_name." ".$user->v_surname,
								'_key' => 'customer_sentrybay_notification',
								'_lang' => $user->v_lang,
								'_keywords' => array(
									'[user_firstname]' => $user->v_first_name,
									'[user_surname]' => $user->v_surname,
									'[user_assets_result_table]' => $resultsTable,
									'[result_count]' => count( $newResults ),
								),
							) );
						}
						
						## Sent SMS
						if( $isSMS && $user->v_mobile ){
							$sms = _send_sms( array(
								'_to' => $user->v_mobile,
								'_body' => '',
								'_key' => 'customer_sentrybay_notification',
								'_keywords' => array(
									'[user_firstname]' => $user->v_first_name,
									'[user_surname]' => $user->v_surname,
									'[user_assets_result_table]' => $resultsTable,
									'[result_count]' => count( $newResults ),
								),
							) );
						}
						
					}
					
				}
				
				$_tracker->update( array( 'v_last_id' => $user->id ) );
			}
			else{
				$_tracker->update( array( 'v_last_id' => 0 ) );
			}
			
			sleep( 10 );
			
			$cnt++;
		}
		
		return 'Cron executed successfully...';
		
	}
	
	
	public function extractResultDemo(){
		
		/*$temp = AssetsResults::all();
		foreach( $temp as $row ){
			$row2 = $row->toArray();
			$v_result_category = $row2['v_result_category'];
			$v_result_category = explode( '(', $v_result_category );
			$v_result_category = trim( $v_result_category[0] );
			$row2['v_result_category'] = $v_result_category;
			$row->update( $row2 );
			_p( $row2 );
		}
		_p( $temp ); exit;*/
		
		$sentrybayCall = new Sentrybay();
		$responseAsset = $sentrybayCall->apiExtractResult( array(
			"MemberNo" => $_REQUEST['MemberNo'],
		) );
		
		foreach( $responseAsset['result']['Member']['Result'] as $row ){
			
			$v_result_title = $row['Attr']['category'];
			$v_result_category = explode( '(', $v_result_title );
			$v_result_category = trim( $v_result_category[0] );
			
			$assetsIns = array(
				'i_user_id'			=> 0,
				'v_user_id'			=> 0,
				'v_result_id'		=> $row['Attr']['id'],
				'v_result_category'	=> $v_result_category,
				'v_result_title'	=> $v_result_title,
				'v_result_source'	=> $row['Attr']['source'],
				'v_result_risk'		=> $row['Attr']['risk'],
				'f_result_percent'	=> $row['Attr']['percent'],
				'v_result'			=> $row['Value'],
				'd_scan_date'		=> $responseAsset['result']['Member']['ScanDate'],
			);
			_p( $assetsIns );
			
		}
		
		_p( $responseAsset ); exit;
		
		return 'Cron executed successfully...';
		
	}
	
}
