<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
	
	##AddedByD
	protected $_section_key, $_section_info;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(){
		$this->middleware('guest', ['except' => 'logout']);
    }
	
	

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data){
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data){
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
	
	
	/** 
	 *
	 * Overridded method : ##AddedByD
	*/
	public function showLoginForm(){
		
		##AddedByD
		$this->_section_key 	= '__LOGIN';
		$this->_section_info 	= _admin_sections( $this->_section_key );
		
		$pass_array = array(
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
		);
		return view('auth.login', $pass_array );
    }
	
	/** 
	 *
	 * Overridded method : ##AddedByD
	*/
	public function showRegistrationForm(){
		
		##AddedByD
		$this->_section_key 	= '__REGISTER';
		$this->_section_info 	= _admin_sections( $this->_section_key );
		
		$pass_array = array(
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
		);
		return view('auth.register', $pass_array );
    }
	
	
	
	
	
}
