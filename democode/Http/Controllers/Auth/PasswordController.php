<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
	
	##AddedByD
	protected $_section_key, $_section_info;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
	
	
	
	/** 
	 *
	 * Overridded method : ##AddedByD
	*/
	public function showLinkRequestForm(){
		
		##AddedByD
		$this->_section_key 	= '__RESET_PASSWORD';
		$this->_section_info 	= _admin_sections( $this->_section_key );
		
		$pass_array = array(
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
		);
		return view('auth.passwords.email', $pass_array );
    }
	
	/** 
	 *
	 * Overridded method : ##AddedByD
	*/
	public function showResetForm( Request $request, $token = null ){
		
		if ( is_null( $token ) ) {
            return $this->getEmail(); // Parent Class Method ##AddedByD
        }
		
		##AddedByD
		$this->_section_key 	= '__RESET_PASSWORD';
		$this->_section_info 	= _admin_sections( $this->_section_key );
		
		$email = $request->input('email');
		
		$pass_array = array(
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'token' => $token,
			'email' => $email,
		);
		return view('auth.passwords.reset', $pass_array );
		
		
    }
}
