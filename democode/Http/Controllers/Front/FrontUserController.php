<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon, 
	SimpleXMLElement, DOMDocument;

use App\FrontUser;
use App\Models\FrontUserLog;
use App\Models\Page;
use App\Models\Country;
use App\Models\Glossary;
use App\Models\TipsType;
use App\Models\RSS;
use App\Models\AssetsResults;

use App\Sentrybay;
use App\TCS;

use App\Http\Requests\Front\LoginRequest;

use PHPMailerAutoload; 
use PHPMailer;

class FrontUserController extends Controller{
	
	## Register
	public function showRegisterForm(){
		
		if( auth()->guard('frontuser')->check() ){
			return redirect( _url('my-account') );
		}
		
		$pass_array = array(
			'_title' 			=> _get_lbl( 'lbl_register' ),
			'_data'				=> array(),
			'_lang_data'		=> array(),
			'_front_contries'	=> _front_contries(),
		);
		return view( 'front.register', $pass_array );
	}
	public function register(){
		
		$RequestData = Request::all();
		
		
		if( !_valid_request( $_SERVER['HTTP_REFERER'] ) ){
			return redirect( _url('login') )->with( 'msg', '0:msg_invalid_request' );
		}
		if( auth()->guard('frontuser')->check() ){
			return redirect( _url('my-account') );
		}
		
		// Check For Validations
		$msg = array();
		$validate = array(
			'rules' => array(
				'v_first_name' 	=> 'required|max:255',
				'v_surname' 	=> 'required|max:255',
				'v_phone' 		=> 'required|max:255',
				'v_email' 		=> 'required|email|unique:tbl_users',
				'v_tcs_id' 		=> 'required',
				//'v_tcs_id' 		=> 'required|unique:tbl_users',
				'v_address_1' 	=> 'required|max:255',
				'v_address_2' 	=> 'required|max:255',
				'v_zipcode' 	=> 'required|max:255',
				'v_city' 		=> 'required|max:255',
				// 'i_country' 	=> 'required',
				'password' 		=> 'required',
			),
			'msgs' => array(
				'v_first_name.required' => _get_lbl( 'lbl_first_name' ),
				'v_surname.required' 	=> _get_lbl( 'lbl_surname' ),
				'v_phone.required' 		=> _get_lbl( 'lbl_telephone' ),
				'v_email.required' 		=> _get_lbl( 'lbl_email' ),
				'v_tcs_id.required' 	=> _get_lbl( 'lbl_membership_no' ),
				'v_address_1.required' 	=> _get_lbl( 'lbl_register_address_1' ),
				'v_address_2.required' 	=> _get_lbl( 'lbl_register_address_2' ),
				'v_zipcode.required' 	=> _get_lbl( 'lbl_post_code' ),
				'v_city.required' 		=> _get_lbl( 'lbl_city' ),
				//'i_country.required' 	=> _get_lbl( 'lbl_country' ),
				'password.required' 	=> _get_lbl( 'lbl_password' ),
			)
		);
		if( $RequestData['v_alert'] == 'sms' ){
			$validate['rules']['v_mobile'] = 'required';
			$validate['msgs']['v_mobile.required'] = _get_lbl( 'lbl_mobile' );
		}
		$validator = Validator::make( $RequestData, $validate['rules'], $validate['msgs'] );
		if( $validator->fails() ){
			$temp = '';
			foreach( $validator->errors()->messages() as $rowMsgKey => $rowMsg ){
				if( $rowMsgKey == 'v_email' && $rowMsg[0] != _get_lbl( 'lbl_email' ) ){
					$msg[] = '0:msg_err_emailexists';
				}
				else if( $rowMsgKey == 'v_tcs_id' && $rowMsg[0] != _get_lbl( 'lbl_membership_no' ) ){
					$msg[] = '0:msg_err_membershipnoexists';
				}
				else{
					$temp .= '<li>'.$rowMsg[0].'</li>';
				}
			}
			$temp = $temp ? '<ul>'.$temp.'</ul>' : '';
			if( $temp ){
				$msg[] = '0:'._get_lbl( 'msg_require_fields' )."<br />".$temp;
			}
			return redirect( _url('register') )->with( 'msg', $msg )->withInput( Request::input() );
		}
		
		
		$RequestData['v_tcs_id'] = str_replace( '.', '', $RequestData['v_tcs_id'] );
		$RequestData['v_tcs_id'] = str_replace( ' ', '', $RequestData['v_tcs_id'] );
		
		if( in_array( strtolower( $RequestData['v_email'] ), array(
				'deven.crestinfotech@gmail.com',
				'user_1@i-surance.ch',
				'user_2@i-surance.ch',
				'user_3@i-surance.ch',
				'user_4@i-surance.ch',
				'user_5@i-surance.ch',
				'user_6@i-surance.ch',
				'user_7@i-surance.ch',
				'user_8@i-surance.ch',
				'user_9@i-surance.ch',
				'user_10@i-surance.ch',
				
				'philipp.hoyer@i-surance.ch',
				'jens.schaedler@i-surance.ch',
				'pascale.schaedler@i-surance.ch',
				'anais.baltsavias@i-surance.de',
				'elisa.darold@i-surance.de',
				'elisa.simeoni@i-surance.de',
				'franca.kruse@i-surance.de',
				'giulia.salezze@i-surance.de',
				'gustavo.sanchez@i-surance.de',
				'helene.launay@i-surance.de',
				'lisa.hofmann@i-surance.de',
				'luana.Naquin@i-surance.de',
				'margherita.mazzoni@i-surance.de',
				'Mariken.vangulpen@i-surance.de',
				'martina.deltetto@i-surance.de',
				'paul.ackermann@i-surance.de',
				'robert.horst@i-surance.de',
				'sarah.hamon@i-surance.de',
				'valerio.adragna@i-surance.de',
				'zidane.momo@i-surance.de',
				'jeanne.romero@i-surance.es',
				'marion.rapin@i-surance.es',
				'ruxandra.bogdan@i-surance.fr',
				
			) ) ){
		}
		else if( _get_setting('TCS_ENABLE') ){
			$apiTCs = new TCS();
			$apiTCs->checkMemberValidation( array(
				'personalReference' => $RequestData['v_tcs_id'],
			) );
			if( $apiTCs->response['succ'] ){
				if( $apiTCs->response['result']['errorCode'] != '00' ){
					return redirect( _url('register') )->with( 'msg', '0:msg_err_occurred' );
				}
				else{
					if( $apiTCs->response['result']['eligibilityStatus'] != '21' ){
						// return redirect( _url('register') )->with( 'msg', '0:msg_err_invalid_membership_no' );
						return redirect( _url('register') )->with( 'msg', '0:msg_tcs_response_'.$apiTCs->response['result']['eligibilityStatus'] );
					}
					// msg_tcs_response_01, msg_tcs_response_11, msg_tcs_response_12, msg_tcs_response_21, msg_tcs_response_22
				}
			}
		}
		
		
		
		$RequestData['password'] = Hash::make( $RequestData['password'] );
		$RequestData['e_status'] = 'active';
		
		$row = FrontUser::create( $RequestData );
		
		$New = FrontUser::find( $row->id );
		$New->v_lang = _lang();
		$New->v_user_id = str_pad( $row->id, 4, "1000", STR_PAD_LEFT );
		$New->save();
		
		$mail = _send_mail( array(
			'_to' => $New->v_email,
			'_to_name' => $New->v_first_name." ".$New->v_surname,
			'_key' => 'customer_register',
			'_keywords' => array(
				'[user_firstname]' => $New->v_first_name,
				'[user_surname]' => $New->v_surname
			),
		) );
		
		return redirect( _url('register') )->with( 'msg', '1:msg_succ_register' );
		
	}
	
	## Login
	public function showLoginForm(){
		
		if( auth()->guard('frontuser')->check() ){
			return redirect( _url('my-account') );
		}
		
		$pass_array = array(
			'_title' 			=> _get_lbl( 'lbl_login' ),
			'_data'				=> array(),
			'_lang_data'		=> array(),
			'_front_contries'	=> _front_contries(),
		);
		return view( 'front.login', $pass_array );
		
	}
	public function login( LoginRequest $request ){
		
		if( !_valid_request( $_SERVER['HTTP_REFERER'] ) ){
			return redirect( _url('login') )->with( 'msg', '0:msg_invalid_request' );
		}
		
		$remember_me = false;
		
		$RequestData = $request->all();
		
		if( isset( $RequestData['remember_me'] ) && $RequestData['remember_me'] ){
			$remember_me = true;
		}
		
		$auth = auth()->guard('frontuser');
		
		if( $auth->attempt( $request->only( 'v_email', 'password' ), $remember_me ) ){
			
			if( in_array( strtolower( $RequestData['v_email'] ), array(
					'deven.crestinfotech@gmail.com',
					'user_1@i-surance.ch',
					'user_2@i-surance.ch',
					'user_3@i-surance.ch',
					'user_4@i-surance.ch',
					'user_5@i-surance.ch',
					'user_6@i-surance.ch',
					'user_7@i-surance.ch',
					'user_8@i-surance.ch',
					'user_9@i-surance.ch',
					'user_10@i-surance.ch',
					
					'philipp.hoyer@i-surance.ch',
					'jens.schaedler@i-surance.ch',
					'pascale.schaedler@i-surance.ch',
					'anais.baltsavias@i-surance.de',
					'elisa.darold@i-surance.de',
					'elisa.simeoni@i-surance.de',
					'franca.kruse@i-surance.de',
					'giulia.salezze@i-surance.de',
					'gustavo.sanchez@i-surance.de',
					'helene.launay@i-surance.de',
					'lisa.hofmann@i-surance.de',
					'luana.Naquin@i-surance.de',
					'margherita.mazzoni@i-surance.de',
					'Mariken.vangulpen@i-surance.de',
					'martina.deltetto@i-surance.de',
					'paul.ackermann@i-surance.de',
					'robert.horst@i-surance.de',
					'sarah.hamon@i-surance.de',
					'valerio.adragna@i-surance.de',
					'zidane.momo@i-surance.de',
					'jeanne.romero@i-surance.es',
					'marion.rapin@i-surance.es',
					'ruxandra.bogdan@i-surance.fr',
					
				) ) ){
				
			}
			else if( _get_setting('TCS_ENABLE') ){
				
				$apiTCs = new TCS();
				$apiTCs->checkMemberValidation( array(
					'personalReference' => _user( 'v_tcs_id' ),
				) );
				if( $apiTCs->response['succ'] ){
					if( $apiTCs->response['result']['errorCode'] != '00' ){
						auth()->guard('frontuser')->logout();
						return redirect( _url('login') )->with( 'msg', '0:msg_err_occurred' );
					}
					else{
						if( $apiTCs->response['result']['eligibilityStatus'] != '21' ){
							auth()->guard('frontuser')->logout();
							return redirect( _url('login') )->with( 'msg', '0:msg_tcs_response_'.$apiTCs->response['result']['eligibilityStatus'] );
						}
						// msg_tcs_response_01, msg_tcs_response_11, msg_tcs_response_12, msg_tcs_response_21, msg_tcs_response_22
					}
				}
			}
			
			if( auth()->guard('frontuser')->user()->e_status == 'inactive' ){
				auth()->guard('frontuser')->logout();
				return redirect( _url('login') )->with( 'msg', '0:msg_account_inactive' );
			}
			
			if( auth()->guard('frontuser')->check() ) {
				
				Session::set('lang', _user( 'v_lang' ) );
				
				FrontUserLog::create([
					'i_user_id' => auth()->guard('frontuser')->user()->id,
					'login_at' 	=> date('Y-m-d H:i:s'),
					'v_ip' => Request::ip(),
				]);
			}
			
			return redirect( _url('my-account') );
		}
		else {
			return redirect( _url('login') )->with( 'msg', '0:msg_invalid_login' );
		}
		
	}
	
	## Forgot Password
	public function showForgotPasswordForm(){
		
		if( auth()->guard('frontuser')->check() ){
			return redirect( _url('my-account') );
		}
		
		$pass_array = array(
			'_title' 			=> _get_lbl( 'lbl_forgot_password_title' ),
			'_data'				=> array(),
			'_lang_data'		=> array(),
		);
		return view( 'front.forgot-password', $pass_array );
		
	}
	public function forgotpassword(){
		
		if( !_valid_request( $_SERVER['HTTP_REFERER'] ) ){
			return redirect( _url('login') )->with( 'msg', '0:msg_invalid_request' );
		}
		
		if( auth()->guard('frontuser')->check() ){
			return redirect( _url('my-account') );
		}
		
		$RequestData = Request::all();
		
		$check = FrontUser::where( 'v_email', '=', $RequestData['v_email'] )->first();
		if( !$check->count() ){
			return redirect( _url('forgot-password') )->with( 'msg', '0:msg_err_no_account' );
		}
		else{
			
			$check = FrontUser::where( 'v_email', '=', $RequestData['v_email'] )->first();
			
			$reset_token = md5( $check->id.time() );
			
			$mail = _send_mail( array(
				'_to' => $check->v_email,
				'_to_name' => $check->v_first_name." ".$check->v_surname,
				'_key' => 'customer_forgot_password',
				'_keywords' => array(
					'[user_firstname]' => $check->v_first_name,
					'[user_surname]' => $check->v_surname,
					'[forgot_password_link]' => _url('reset-password').'/'.$reset_token,
				),
			) );
			
			if( $mail ){
				
				$check->reset_token = $reset_token;
				$check->save();
			
				return redirect( _url('forgot-password') )->with( 'msg', '1:msg_succ_reset_password_link_sent' );
			}
			else{
				return redirect( _url('forgot-password') )->with( 'msg', '0:msg_err_email_can_not_sent' );
			}
		}
		
	}
	
	## Reset Password
	public function showResetPasswordForm( $reset_token ){
		
		if( !trim( $reset_token ) ){
			return redirect('login')->with('msg', '0:err_msg_something_went_wrong');
		}
		
		$check = FrontUser::where( 'reset_token', '=', $reset_token )->first();
		if( !$check->count() ){
			return redirect('login')->with('msg', '0:msg_err_invalid_reset_password_token');
		}
		
		$pass_array = array(
			'_title' 			=> _get_lbl( 'lbl_reset_password_title' ),
			'_data'				=> array(),
			'_lang_data'		=> array(),
		);
		return view( 'front.reset-password', $pass_array );
		
	}
	public function resetpassword( $reset_token ){
		
		$RequestData = Request::all();
		
		
		if( !_valid_request( $_SERVER['HTTP_REFERER'] ) ){
			return redirect( _url('login') )->with( 'msg', '0:msg_invalid_request' );
		}
		
		if( !trim( $reset_token ) ){
			return redirect('login')->with('msg', '0:err_msg_something_went_wrong');
		}
		
		$check = FrontUser::where( 'reset_token', '=', $reset_token )->first();
		if( !$check->count() ){
			return redirect('login')->with('msg', '0:msg_err_invalid_reset_password_token');
		}
		
		// Check For Validations
		$msg = array();
		$validate = array(
			'rules' => array(
				'password' => 'required|confirmed',
				'password_confirmation' => 'required',
			),
			'msgs' => array(
				'password.required' => _get_lbl( 'lbl_new_password' ),
				'password_confirmation.required' => _get_lbl( 'lbl_confirm_password' ),
			)
		);
		
		$validator = Validator::make( $RequestData, $validate['rules'], $validate['msgs'] );
		if( $validator->fails() ){
			$temp = '';
			foreach( $validator->errors()->messages() as $rowMsgKey => $rowMsg ){
				if( ( $rowMsgKey == 'password' && $rowMsg[0] == _get_lbl( 'lbl_new_password' ) )
					|| $rowMsgKey == 'password_confirmation' && $rowMsg[0] == _get_lbl( 'lbl_confirm_password' ) ){
					$temp .= '<li>'.$rowMsg[0].'</li>';
				}
				else{
					$msg[] = '0:'._get_lbl( 'msg_invalid_passwords' );
				}
			}
			$temp = $temp ? '<ul>'.$temp.'</ul>' : '';
			if( $temp ){
				$msg[] = '0:'._get_lbl( 'msg_require_fields' )."<br />".$temp;
			}
			return redirect( _url( 'reset-password/'.$reset_token ) )->with( 'msg', $msg )->withInput( Request::input() );
		}
		
		$check->reset_token = '';
		$check->password = Hash::make( $RequestData['password'] );
		$check->save();
		
		return redirect('login')->with('msg', '1:msg_succ_your_account_password_has_been_reset');
		
	}
	
	## Logout
	public function logout() {
		
		$lastLogin = FrontUserLog::where( "i_user_id", "=", auth()->guard('frontuser')->user()->id )
			->orderBy('id','DESC')
			->first();
		if( $lastLogin->count() ) {
			$lastLogin->logout_at = date('Y-m-d H:i:s');
			$lastLogin->save();
		}
		
		if( !auth()->guard('frontuser')->check() ){
			redirect( _url('/') ); 
		}
		
		auth()->guard('frontuser')->logout();
		
		return redirect( _url('login') )->with( 'msg', '1:msg_logout' );;
		
	}
	
	## Change Password
	public function changePassword(){
		
		$RequestData = Request::all();
		
		if( !_valid_request( $_SERVER['HTTP_REFERER'] ) ){
			return redirect( _url('login') )->with( 'msg', '0:msg_invalid_request' );
		}
		
		
		// Check For Validations
		$msg = array();
		$validate = array(
			'rules' => array(
				'password' => 'required|confirmed',
				'password_confirmation' => 'required',
			),
			'msgs' => array(
				'password.required' => _get_lbl( 'lbl_new_password' ),
				'password_confirmation.required' => _get_lbl( 'lbl_confirm_password' ),
			)
		);
		
		$validator = Validator::make( $RequestData, $validate['rules'], $validate['msgs'] );
		if( $validator->fails() ){
			$temp = '';
			foreach( $validator->errors()->messages() as $rowMsgKey => $rowMsg ){
				if( ( $rowMsgKey == 'password' && $rowMsg[0] == _get_lbl( 'lbl_new_password' ) )
					|| $rowMsgKey == 'password_confirmation' && $rowMsg[0] == _get_lbl( 'lbl_confirm_password' ) ){
					$temp .= '<li>'.$rowMsg[0].'</li>';
				}
				else{
					$msg[] = '0:'._get_lbl( 'msg_invalid_passwords' );
				}
			}
			$temp = $temp ? '<ul>'.$temp.'</ul>' : '';
			if( $temp ){
				$msg[] = '0:'._get_lbl( 'msg_require_fields' )."<br />".$temp;
			}
			return redirect( _url('my-account') )->with( 'msg', $msg )->withInput( Request::input() );
		}
		
		
		$check = FrontUser::find( _user( 'id' ) );
		if( !$check->count() ){
			return redirect('login')->with('msg', '0:msg_err_no_account');
		}
		
		$check->password = Hash::make( $RequestData['password'] );
		$check->save();
		return redirect('my-account')->with('msg', '1:msg_succ_your_account_password_has_been_reset');
		
	}
	
	## Edit Profile
	public function editProfile(){
		$_user = _user()->toArray();
		$pass_array = array(
			'_title' 				=> _get_lbl( 'lbl_member_portal' ),
			'_data'					=> array(),
			'_lang_data'			=> array(),
			'_user'					=> $_user,
		);
		return view( 'front.edit-profile', $pass_array );
	}
	public function saveProfile(){
		
		if( !_valid_request( $_SERVER['HTTP_REFERER'] ) ){
			return redirect( _url('login') )->with( 'msg', '0:msg_invalid_request' );
		}
		
		$RequestData = Request::all();
		
		// Email Validation
		$check = FrontUser::where( 'v_email', '=', $RequestData['v_email'] )->where( 'id', '!=', _user( 'id' ) )->get();
		if( $check->count() ){
			return redirect( _url( 'edit-profile' ) )->with( 'msg', '0:msg_err_emailexists' );
		}
		
		// Save User Information
		$user = FrontUser::find( _user( 'id' ) )->update( $RequestData );
		
		$msgs = array(
			'1:msg_succ_profile_updated',
		);
		return redirect( _url( 'my-account' ) )->with( 'msg', $msgs );
		
	}
	
	
	## My Account
	public function myAccount(){
		if( isset( $_REQUEST['sby'] ) && $_REQUEST['sby'] == 'apiExtractAsset' ){
			$sentrybayCall = new Sentrybay();
			$responseAsset = $sentrybayCall->apiExtractAsset( array(
				"MemberNo" => _user( 'v_user_id' )
			) );
			_p( $responseAsset ); 
			exit;
		}
		if( isset( $_REQUEST['sby'] ) && $_REQUEST['sby'] == 'apiExtractResult' ){
			$sentrybayCall = new Sentrybay();
			$responseAsset = $sentrybayCall->apiExtractResult( array(
				"MemberNo" => _user( 'v_user_id' ),
			) );
			_p( $sentrybayCall ); exit;
		}
		
		if( isset( $_REQUEST['sby'] ) && $_REQUEST['sby'] == 'apiSummaryInfo' ){
			$sentrybayCall = new Sentrybay();
			$responseAsset = $sentrybayCall->apiSummaryInfo( array(
				"MemberNo" => _user( 'v_user_id' ),
			) );
			_p( $responseAsset ); exit;
		}
				
		$GlossaryArr = Glossary::front( 1 );
		$TipsTypeArr = TipsType::front();
		$RSSArr = RSS::getFrontList();
		$_user = _user()->toArray();
		
		$_userAssets = _user()->getAssets( array(
			"MemberNo" => _user( 'v_user_id' )
		) );
		// _p( $_userAssets ); exit;
		
		$_userAssetsResults = _user()->getAssetsResults();
		//_p( $_userAssetsResults ); exit;
		
		$_userGetRiskLevel = _user()->getRiskLevel();
		// _p( $_userGetRiskLevel ); exit;
		
		$pass_array = array(
			'_title' 				=> _get_lbl( 'lbl_member_portal' ),
			'_data'					=> array(),
			'_lang_data'			=> array(),
			'GlossaryArr'			=> $GlossaryArr,
			'TipsTypeArr'			=> $TipsTypeArr,
			'RSSArr'				=> $RSSArr,
			'_user'					=> $_user,
			'_userAssets'			=> $_userAssets,
			'_userAssetsResults'	=> $_userAssetsResults,
			'_userGetRiskLevel'		=> $_userGetRiskLevel,
		);
		return view( 'front.my-account', $pass_array );
	}
	
	## Save My Account
	public function saveMyAccount(){
		
		if( !_valid_request( $_SERVER['HTTP_REFERER'] ) ){
			return redirect( _url('login') )->with( 'msg', '0:msg_invalid_request' );
		}
		
		$RequestData = Request::all();
		
		$RequestData['d_birthdate'] = date( 'Y-m-d', strtotime( $RequestData['d_birthdate'] ) );
		
		// Email Validation
		$check = FrontUser::where( 'v_email', '=', $RequestData['v_email'] )->where( 'id', '!=', _user( 'id' ) )->get();
		if( $check->count() ){
			return redirect( _url( 'my-account' ) )->with( 'msg', '0:msg_err_emailexists' );
		}
		
		if( is_numeric( $RequestData['remove_asset_id'] ) && $RequestData['remove_asset_id'] >= 0 ){
			unset( $RequestData['l_assets'][ $RequestData['remove_asset_id'] ] );
		}
		
		// Save User Information
		$userID = _user( 'id' );
		$user = FrontUser::find( $userID );
		$user->update( $RequestData );
		$userInfo = $user->toArray();
		
		$msgs = array(
			// '1:msg_succ_profile_updated',
		);
		
		$Country = array();
		if( _user( 'v_lang' ) ){
			$Country[] = _user( 'v_lang' );
		}
		if( _user( 'country_code' ) ){
			$Country[] = _user( 'country_code' );
		}
		
		// Subscribe To SentryBay
		$sentrybayCall = new Sentrybay();
		$subscribeResponse = $sentrybayCall->apiSubscribe( array(
			"profile" => array(
				'MemberNo' => $userInfo['v_user_id'],
				'FirstName' => $userInfo['v_first_name'],
				'LastName' => $userInfo['v_surname'],
				'Email' => $userInfo['v_email'],
				'DOB' => $userInfo['d_birthdate'],
				'Products' => 'DMS',
				'Country' => implode( '-', $Country ),
			),
		) );
		
		if( $subscribeResponse['succ'] ){
			
			if( $user->i_sentrybay ){
				$msgs[] = '1:msg_succ_sentrybay_profile_updated';
			}
			else{
				$msgs[] = '1:msg_succ_sentrybay_subscribe';
			}
			
			$user->i_sentrybay = 1;
			$user->save();
			
			if( isset( $RequestData['l_assets'] ) && count( $RequestData['l_assets'] ) ){
				$sentrybayCall = new Sentrybay();
				$updateAssetResponse = $sentrybayCall->apiUpdateAsset( array(
					"MemberNo" => _user( 'v_user_id' ),
					"asset" => $RequestData['l_assets'],
				) );
				if( $updateAssetResponse['succ'] ){
					$msgs[] = '1:msg_succ_sentrybay_asset_update';
				}
				else{
					$msgs[] = '0:msg_err_sentrybay_asset_cannot_update';
					$msgs[] = $response['succ'].':'.$response['msg'];
				}
			}
		}
		else{
			$msgs[] = '0:msg_err_sentrybay_cannot_subscribe';
			$msgs[] = $response['succ'].':'.$response['msg'];
		}
		
		if( is_numeric( $RequestData['remove_asset_id'] ) && $RequestData['remove_asset_id'] >= 0 ){
			$msgs = array(
				'1:msg_succ_asset_removed'
			);
		}
		
		return redirect( _url( 'my-account' ) )->with( 'msg', $msgs );
		
	}
	
	
	## Send ID Result Deletion Request
	public function deletionRequest(){
		
		if( !_valid_request( $_SERVER['HTTP_REFERER'] ) ){
			return redirect( _url('login') )->with( 'msg', '0:msg_invalid_request' );
		}
		
		$RequestData = Request::all();
		
		$ids = _putval( $RequestData, 'deletion_results', array() );
		
		if( !count( $ids ) ){
			return redirect( _url( 'my-account' ) )->with( 'msg', '0:msg_err_select_atleast_one_record' );
		}
		
		$userID = _user( 'id' );
		$user = FrontUser::find( $userID );
		
		$assetsResults = AssetsResults::where( 'i_user_id', '=', $userID )->whereIn( 'id', $ids )->get();
		if( !$assetsResults->count() ){
			return redirect( _url( 'my-account' ) )->with( 'msg', '0:msg_err_select_atleast_one_record' );
		}
		
		$assetsResults = $assetsResults->toArray();
		
		$resultsTable = 
		'<table class="result_table" cellspacing="0" cellpadding="10" style="width:100%;border:1px solid #e4e4e4;" border="0" >
			<thead>
				<tr>
					<th align="left" style="border:1px solid #e4e4e4;" >User ID</th>
					<th align="left" style="border:1px solid #e4e4e4;" >Result ID</th>
					<th align="left" style="border:1px solid #e4e4e4;" >Risk</th>
					<th align="left" style="border:1px solid #e4e4e4;" >What?</th>
					<th align="left" style="border:1px solid #e4e4e4;" >Where?</th>
					<th align="left" style="border:1px solid #e4e4e4;" >Link</th>
					<th align="left" style="border:1px solid #e4e4e4;" >When?</th>
					<th align="left" style="border:1px solid #e4e4e4;" >View</th>
				</tr>
			</thead>
			<tbody>';
				foreach( $assetsResults as $row ){
					$colorArr = array(
						'High' => 'red',
						'Medium' => 'orange',
						'Low' => 'green',
					);
					$resultsTable .= 
					'<tr>
						<td style="border:1px solid #e4e4e4;" >'.$row['v_user_id'].'</td>
						<td style="border:1px solid #e4e4e4;" >'.$row['v_result_id'].'</td>
						<td style="color:'._putval( $colorArr, $row['v_result_risk'], 'green' ).'; border:1px solid #e4e4e4;">'.$row['v_result_risk'].'</td>
						<td style="border:1px solid #e4e4e4;" >'.$row['v_result_title'].'</td>
						<td style="border:1px solid #e4e4e4;" >'.$row['v_result_source'].'</td>
						<td style="border:1px solid #e4e4e4;" >'._get_host( $row['v_result'] ).'</td>
						<td style="border:1px solid #e4e4e4;" >'.$row['d_scan_date'].'</td>
						<td style="border:1px solid #e4e4e4;" ><a href="'.$row['v_result'].'" >View</a></td>
					</tr>';
				}
				$resultsTable .= 
			'</tbody>
		</table>';
		
		//Deven CCard 1
		//_p( $resultsTable ); exit;
		
		$mail = _send_mail( array(
			'_to' => _get_setting('EMAIL_SENTRYBAY_DELETION_REQUEST'),
			'_to_name' => _get_setting('SITE_NAME'),
			'_key' => 'admin_deletion_request',
			'_keywords' => array(
				'[user_id]' => $user->v_user_id,
				'[user_firstname]' => $user->v_first_name,
				'[user_surname]' => $user->v_surname,
				'[user_assets_result_table]' => $resultsTable,
			),
		) );
		
		if( $mail ){
			$temp = AssetsResults::where( 'i_user_id', '=', $userID )->whereIn( 'id', $ids )->update( array(
				'i_delete' => 1
			) );
			return redirect( _url( 'my-account' ) )->with( 'msg', '1:msg_succ_request_deletion_sent_successfully' );
		}
		else{
			return redirect( _url( 'my-account' ) )->with( 'msg', '0:msg_err_request_deletion_cannot_sent' );
		}
		
	}
	
	## TCS Api Demo
	public function tcsDemo(){
		
		// https://joshtronic.com/2014/07/13/parsing-soap-responses-with-simplexml/
		$apiTCs = new TCS();
		$response = $apiTCs->checkMemberValidation( array(
			'personalReference' => '105199133',
			//'activateCheckNames' => 'true',
			//'effectiveDate' => date('Y-m-d'),
			//'firstName' => 'John',
			//'lastName' => 'Mars',
		) );
		_p( $response ); exit;
		
		if( 0 ){
			
			$str = 
				'<?xml version="1.0" encoding="UTF-8"?>
				<s:envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
				<s:body>
					<wss:getcheckserviceeligibilityv1response xmlns:wss="http://www.boomi.com/connector/wss">
						<wss:checkserviceeligibilityv1responselist>
							<ns1:checkserviceeligibilityresponsev1 xmlns:ns1="checkServiceEligibilityV1Response.boomi.com">
							  <ns1:errorcode>00</ns1:errorcode>
							  <ns1:eligibilitystatus>01</ns1:eligibilitystatus>
							</ns1:checkserviceeligibilityresponsev1>
						</wss:checkserviceeligibilityv1responselist>
					</wss:getcheckserviceeligibilityv1response>
				</s:body>
			</s:envelope>';
			dump( $str );
			
			$xml = simplexml_load_string( $str );
			dump( $xml );
			
			$temp = $xml->children('s', true);
			dump( $temp );
			
			$temp = $temp->body->children('wss', true);
			dump( $temp );
			
			$temp = $temp->getcheckserviceeligibilityv1response;
			dump( $temp );
						
			$temp = $temp->checkserviceeligibilityv1responselist;
			dump( $temp );
			
			$temp = $temp->children('ns1', true);
			dump( $temp );
			
			$temp = $temp->checkserviceeligibilityresponsev1;
			dump( $temp );
			
			echo 'errorcode # '.( $temp->errorcode );
			echo '<br>';
			echo 'eligibilitystatus # '.( $temp->eligibilitystatus );
			
			exit;
			
		
		
			$apiXML = 
			'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wss="http://www.boomi.com/connector/wss" xmlns:chec="checkServiceEligibilityV1Request.boomi.com">
				<soapenv:Header/>
				<soapenv:Body>
					<wss:getCheckServiceEligibilityV1>
						<chec:CheckServiceEligibilityRequestV1>
							<chec:personalReference>103414238</chec:personalReference>
							<chec:serviceGCI>INTERNET_SAFETY</chec:serviceGCI>
							<chec:languageForLabels>F</chec:languageForLabels>
							<chec:serviceCaller>I-Surance</chec:serviceCaller>
						</chec:CheckServiceEligibilityRequestV1>
					</wss:getCheckServiceEligibilityV1>
				</soapenv:Body>
			</soapenv:Envelope>';
			
			/*
			<chec:filterOptions>
				<chec:mapKey>activateCheckNames</chec:mapKey>
				<chec:mapValue>true</chec:mapValue>
			</chec:filterOptions>
			<chec:attributes>
				<chec:mapKey>firstName</chec:mapKey>
				<chec:mapValue>Deven</chec:mapValue>
			</chec:attributes>
			<chec:attributes>
				<chec:mapKey>lastName</chec:mapKey>
				<chec:mapValue>R</chec:mapValue>
			</chec:attributes>*/
			
			
			$serverUrl 	= "https://connect.boomi.com/ws/soap?wsdl=single";
			$username 	= 'isurance-cloud-uat@touringclubsuisse-KGJV2M.SD7DOH';
			$password 	= 'c583278b-064b-49a0-852b-14065cc47722';
			
			$headers = array(
				'Content-Type: text/xml;charset=utf-8'
				//'Content-Type: text/xml'
			);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_TIMEOUT, 30 ); //timeout after 30 seconds
			curl_setopt($ch, CURLOPT_URL, $serverUrl );
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password" );
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY );
			curl_setopt($ch, CURLOPT_POST, 1 );
			curl_setopt($ch, CURLOPT_POSTFIELDS, $apiXML );
			
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers );
			//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false );
			//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
			//curl_setopt($ch, CURLOPT_ENCODING, '' );
			
			$response = curl_exec($ch);
			$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			
			curl_close($ch);
			
			_p( 'Status Code : '.$status_code );
			
			_p( 'Response : ' );
			_p( var_dump( $response ) );
			
			// $data = json_decode( json_encode( simplexml_load_string( $response ) ), TRUE );
			
			_p( simplexml_load_string( $response ) );
			
			exit;
		}
	}
	
}
