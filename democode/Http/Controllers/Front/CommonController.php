<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Admin;

use App\Sentrybay;

use App\Models\HomepageSlider;
use App\Models\Page;
use App\Models\FaqType;


class CommonController extends Controller{
	
	protected $_section_key, $_section_info;
	
	public function __construct(){
	}
	
	public function homepage(){
		$pass_array = array(
			'_title' => 'Homepage',
			'_data' => array(
				'_slider' => HomepageSlider::front(),
			),
		);
		return view( 'front.index', $pass_array );
	}
	
	public function pages( $slug ){
		$data = Page::where( "v_slug", "=", $slug )->get();
		if( !$data->count() ){
			return redirect( _get_setting('SITE_URL') );
		}
		$pass_array = array(
			'_title' => $data[0]->langFront()->v_title,
			'_data'	=> $data[0],
			'_lang_data' => $data[0]->langFront(),
		);
		return view( 'front.page', $pass_array );
	}
	
	public function faqs(){
		$data = Page::where( "v_slug", "=", 'faqs' )->get();
		if( !$data->count() ){
			return redirect( _get_setting('SITE_URL') );
		}
		
		$_faq_types = FaqType::getFrontList();
		$pass_array = array(
			'_title' => $data[0]->langFront()->v_title,
			'_data'	=> $data[0],
			'_lang_data' => $data[0]->langFront(),
			'_faq_types' => $_faq_types,
			
		);
		return view( 'front.faqs', $pass_array );
	}
	
	public function sentrybayDemo( $type ){
		
		$RequestData = Request::all();
		
		$sentrybayCall = new Sentrybay();
		
		## Done
		if( $type == 'apiSubscribe' ){
			
			$fields = array(
				'MemberNo',
				'FirstName',
				'LastName',
				'Email',
				'DOB',
				'Products',
				'Country',
			);
			foreach( $fields as $field ){
				if( !( isset( $RequestData[$field] ) && $RequestData[$field] ) ){ die( $field.' is Required'); }
			}
			$result = $sentrybayCall->apiSubscribe( array(
				"profile" => array(
					'MemberNo' => $RequestData['v_user_id'],
					'FirstName' => $RequestData['v_first_name'],
					'LastName' => $RequestData['v_surname'],
					'Email' => $RequestData['v_email'],
					'DOB' => $RequestData['d_birthdate'],
					'Products' => 'DMS',
					'Country' => _user( 'country_code' ),
				),
			) );
		}
		## Done
		else if( $type == 'apiUnsubscribe' ){
			$fields = array(
				'MemberNo',
			);
			foreach( $fields as $field ){
				if( !( isset( $RequestData[$field] ) && $RequestData[$field] ) ){ die( $field.' is Required'); }
			}
			$result = $sentrybayCall->apiUnsubscribe( array(
				"member_id" => $RequestData['MemberNo'],
			) );
		}
		## Done
		else if( $type == 'apiSummaryInfo' ){
			$fields = array(
				'MemberNo',
			);
			foreach( $fields as $field ){
				if( !( isset( $RequestData[$field] ) && $RequestData[$field] ) ){ die( $field.' is Required'); }
			}
			$result = $sentrybayCall->apiSummaryInfo( array(
				"member_id" => $RequestData['MemberNo'],
			) );
		}
		
		## Done
		else if( $type == 'apiUpdateAsset' ){
			
			$fields = array(
				'MemberNo',
				//'FirstName',
				//'LastName',
				//'Email',
				//'DOB',
				//'Products',
				//'Country',
			);
			foreach( $fields as $field ){
				if( !( isset( $RequestData[$field] ) && $RequestData[$field] ) ){ die( $field.' is Required'); }
			}
			
			/*
			<Asset>
				<CreditCard label="visa">1111333322223333</CreditCard>
				<CreditCard label="master">4444555566667777</CreditCard>
				<BankAccount label="barclayes" action="in">
					<AccountNumber>123456</AccountNumber>
					<SortCode>200496</SortCode>
				</BankAccount>
				<BankAccount label="hsbc" action="out">
					<AccountNumber>123456</AccountNumber>
					<SortCode>200496</SortCode>
				</BankAccount>
				<BankDetail label="hsbc" action="in">
					<IBAN>123456</IBAN>
					<BIC>200496</BIC>
				</BankDetail>
				<Address label="home" action="in">
					<Addr1>31 High Road</Addr1>
					<Addr2></Addr2>
					<Addr3></Addr3>
					<City>London</City>
					<ZipCode>EC2 9UN</ZipCode>
					<Country>United Kingdom</Country>
				</Address>
				<DOB label="myself" action="in">1980-04-21</DOB>
				<Phone label="home" action="in">02078823456</Phone>
				<Phone label="work" action="in">02078827890</Phone>
				<Mobile label="iPhone" action="in">07958823456</Mobile>
				<DrivingLicense label="my brother in law" action="in">DL123456</DrivingLicense>
				<Passport label="myself" action="in">Passport Number</Passport>
				<IDCard label="NHS number" action="in">Identification Number</IDCard>
				<SSN label="mine" action="in">SSN or NI, etc</SSN>
				<Email label="work" action="in">search@acme.com</Email>
				<Email label="personal" action="in">search@gmail.com</Email>
				<Email label="social" action="in">search@yahoo.com</Email>
			</Asset>
			*/
			
			$assetXml = '<?xml version="1.0" encoding="UTF-8"?>
				<Members>
					<member>
						<memberno>'.$RequestData['MemberNo'].'</memberno>
						<Asset>
							<Email label="work" action="in">deven.crestinfotech@gmail.com</Email>
						</Asset>
					</member>
					<Extra>
						<Extra1>Extra 1</Extra1>
						<Extra2>Extra 2</Extra2>
					</Extra>
				</Members>';
			
			$result = $sentrybayCall->apiUpdateAsset( array(
				"asset" => $assetXml,
			) );
			
			/*[response] => Array
                (
                    [status_code] => 0
                    [result] => Array
                        (
                            [MemberNo] => deven001
                            [status_code] => 0
                            [status_message] => Success
                            [Extra] => Array
                                (
                                    [Extra1] => Extra 1
                                    [Extra2] => Extra 2
                                )

                        )

                )*/
			
		}
		## Done
		else if( $type == 'apiAmendAsset' ){
			$fields = array(
				'MemberNo',
				//'FirstName',
				//'LastName',
				//'Email',
				//'DOB',
				//'Products',
				//'Country',
			);
			foreach( $fields as $field ){
				if( !( isset( $RequestData[$field] ) && $RequestData[$field] ) ){ die( $field.' is Required'); }
			}
			
			/*
			<Asset>
				<CreditCard label="visa">1111333322223333</CreditCard>
				<CreditCard label="master">4444555566667777</CreditCard>
				<BankAccount label="barclayes" action="in">
					<AccountNumber>123456</AccountNumber>
					<SortCode>200496</SortCode>
				</BankAccount>
				<BankAccount label="hsbc" action="out">
					<AccountNumber>123456</AccountNumber>
					<SortCode>200496</SortCode>
				</BankAccount>
				<BankDetail label="hsbc" action="in">
					<IBAN>123456</IBAN>
					<BIC>200496</BIC>
				</BankDetail>
				<Address label="home" action="in">
					<Addr1>31 High Road</Addr1>
					<Addr2></Addr2>
					<Addr3></Addr3>
					<City>London</City>
					<ZipCode>EC2 9UN</ZipCode>
					<Country>United Kingdom</Country>
				</Address>
				<DOB label="myself" action="in">1980-04-21</DOB>
				<Phone label="home" action="in">02078823456</Phone>
				<Phone label="work" action="in">02078827890</Phone>
				<Mobile label="iPhone" action="in">07958823456</Mobile>
				<DrivingLicense label="my brother in law" action="in">DL123456</DrivingLicense>
				<Passport label="myself" action="in">Passport Number</Passport>
				<IDCard label="NHS number" action="in">Identification Number</IDCard>
				<SSN label="mine" action="in">SSN or NI, etc</SSN>
				<Email label="work" action="in">search@acme.com</Email>
				<Email label="personal" action="in">search@gmail.com</Email>
				<Email label="social" action="in">search@yahoo.com</Email>
			</Asset>
			*/
			
			$assetXml = '<?xml version="1.0" encoding="UTF-8"?>
				<Members>
					<member>
						<memberno>'.$RequestData['MemberNo'].'</memberno>
						<Asset>
							<Email label="work" action="in">deven.crestinfotech@gmail.com</Email>
						</Asset>
					</member>
					<Extra>
						<Extra1>Extra 1</Extra1>
						<Extra2>Extra 2</Extra2>
					</Extra>
				</Members>';
			
			$result = $sentrybayCall->apiAmendAsset( array(
				"asset" => $assetXml,
			) );
			
			/*[response] => Array
                (
                    [status_code] => 0
                    [result] => Array
                        (
                            [MemberNo] => deven001
                            [status_code] => 0
                            [status_message] => Success
                            [Extra] => Array
                                (
                                    [Extra1] => Extra 1
                                    [Extra2] => Extra 2
                                )

                        )

                )*/
			
		}
		## Output Error
		else if( $type == 'apiExtractAsset' ){
			$fields = array(
				'MemberNo',
			);
			foreach( $fields as $field ){
				if( !( isset( $RequestData[$field] ) && $RequestData[$field] ) ){ die( $field.' is Required'); }
			}
			
			$assetXml = '<?xml version="1.0" encoding="UTF-8"?>
			<Extra>
				<Extra1>Extra 1</Extra1>
				<Extra2>Extra 2</Extra2>
			</Extra>';
			
			
			$assetXml = $RequestData['MemberNo'];
			
			/*<Asset>
							<Email label="work" action="in">deven.crestinfotech@gmail.com</Email>
						</Asset>*/
			/*<member>
						<memberno>'.$RequestData['MemberNo'].'</memberno>
					</member>*/
			$extraXml = '<?xml version="1.0" encoding="UTF-8"?>
			<Extra>
				<Extra1>Extra 1</Extra1>
				<Extra2>Extra 2</Extra2>
			</Extra>';
			
			$result = $sentrybayCall->apiExtractAsset( array(
				"member_id" => $RequestData['MemberNo'],
				"extra" => $extraXml,
			) );
			
			
		}
		## Done
		else if( $type == 'apiExtractResult' ){
			$fields = array(
				'MemberNo',
			);
			foreach( $fields as $field ){
				if( !( isset( $RequestData[$field] ) && $RequestData[$field] ) ){ die( $field.' is Required'); }
			}
			$result = $sentrybayCall->apiExtractResult( array(
				"member_id" => $RequestData['MemberNo'],
			) );
			/*
			[response] => Array
                (
                    [status_code] => 0
                    [result] => Array
                        (
                            [@attributes] => Array
                                (
                                    [brand] => isurance
                                )

                            [Member] => Array
                                (
                                    [MemberNo] => deven001
                                )

                        )

                )
			*/
		}
		
		## Done
		else if( $type == 'apiNotification' ){
			$fields = array(
				'MemberNo',
			);
			foreach( $fields as $field ){
				if( !( isset( $RequestData[$field] ) && $RequestData[$field] ) ){ die( $field.' is Required'); }
			}
			
			$contextXml = '<?xml version="1.0" encoding="UTF-8"?>
				<Members>
					<member>
						<memberno>'.$RequestData['MemberNo'].'</memberno>
						<Notification>
							<Ignore hitid="1378">http://url1.we.found.before.1</Ignore>
							<Ignore hitid="1379">http://url1.we.found.before.2</Ignore>
						</Notification>
					</member>
					<Extra>
						<Extra1>Extra 1</Extra1>
						<Extra2>Extra 2</Extra2>
					</Extra>
				</Members>';
			
			$result = $sentrybayCall->apiNotification( array(
				"context" => $contextXml,
			) );
			
			/*
			[response] => Array
                (
                    [status_code] => 0
                    [result] => Array
                        (
                            [MemberNo] => deven001
                            [status_code] => 0
                            [status_message] => Success
                            [Extra] => Array
                                (
                                    [Extra1] => Extra 1
                                    [Extra2] => Extra 2
                                )

                        )

                )
			*/
			
		}
		
		else if( $type == 'apiReportResult' ){
			$fields = array(
				'MemberNo',
			);
			foreach( $fields as $field ){
				if( !( isset( $RequestData[$field] ) && $RequestData[$field] ) ){ die( $field.' is Required'); }
			}
			
			$resultXml = '<?xml version="1.0" encoding="UTF-8"?>
				<scanresult type="Alert">	
					<member>
						<memberno>'.$RequestData['MemberNo'].'</memberno>
						<scandate>2012-01-16 13:48:12</scandate>
						<result id="484" category="Credit Card(visa)" source="publicweb" risk="High" percent="80.96">http://www.dodge.com/+1/button/</result>
						<result id="485" category="Email(work)" source="publicweb" risk="Low" percent="8.00">http://icann-registrars-life.dotandco.net/archives/2011/10/01/C30.html</result>
					</member>
					<member>
						<memberno>11990679</memberno>
						<scandate>2012-01-16 13:50:06</scandate>
						<result id="486" category="Bank Account(barclays), Address(home)" source="publicweb" risk="High" percent="83.68">http://www.ncbi.nlm.nih.gov/gene/111</result>
					</member>
				</scanresult>';
				
			$resultXml = '<?xml version="1.0" encoding="UTF-8"?>
				<scanresult type="Alert">	
					<member>
						<memberno>11990679</memberno>
						<scandate>2012-01-16 13:50:06</scandate>
						<result id="486" category="Bank Account(barclays), Address(home)" source="publicweb" risk="High" percent="83.68">http://www.ncbi.nlm.nih.gov/gene/111</result>
					</member>
				</scanresult>';
			
			$result = $sentrybayCall->apiReportResult( array(
				"result" => $resultXml,
			) );
		}
		
		_p( $sentrybayCall );
		
	}
	
	public function smsDemo(){
		
		// http://api.clickatell.com/http/sendmsg?user=philipp.hoyer.i-surance&password=i-surance_2017&api_id=3640166&to=41788710240&text=Testing%20IDprotect%20Message&from=IDProtect
		
		$url = 'http://api.clickatell.com/http/sendmsg';
		
		$sms = _curl( $url, array(
			'user' => 'philipp.hoyer.i-surance',
			'password' => 'i-surance_2017',
			'api_id' => '3640166',
			'to' => '919998120220',
			'text' => 'Testing SMS from the IDProtect',
			'from' => 'IDProtect',
		) );
		
		_p( $sms );
		
	}
	
}
