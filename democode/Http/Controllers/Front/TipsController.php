<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\TipsType;
use App\Models\Tips;

class TipsController extends Controller{
	
	public function index( $slug ){
		$data = TipsType::where( "v_slug", "=", $slug )->get();
		if( !$data->count() ){
			return redirect( _url('my-account') );
		}
		
		$pass_array = array(
			'_title' 		=> $data[0]->langFront()->v_title,
			'_data'			=> $data[0],
			'_lang_data' 	=> $data[0]->langFront(),
		);
		return view( 'front.tips', $pass_array );
	}
	
}
