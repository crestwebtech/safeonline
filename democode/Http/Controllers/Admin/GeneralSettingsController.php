<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\GeneralSettings, 
	App\Models\Language;

class GeneralSettingsController extends Controller{
	
	protected $_section_key, $_section_info;
	
	public function __construct(){
		
		$this->_section_key = '__GENERAL_SETTINGS';
		$this->_section_info = _admin_sections( $this->_section_key );
		
	}
	
	public function index(){
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] == 'Update' ){
			
			unset( $Request_Data['_token'] );
			unset( $Request_Data['submit_btn'] );
						
			$avoid_keys = array(
				'_token', 'submit_btn', 'v_old_files'
			);
			
			$file_keys = array(
				'LOGO' 		=> 'logo',
				'FAVICON' 	=> 'logo',
			);
			
			foreach( $Request_Data as $k => $v ){
				
				if( in_array( $k, $avoid_keys ) ) continue;
				
				if( isset( $file_keys[ $k ] ) ){
					
					if( !isset( $Request_Data[ $k ] ) ){ continue; }
					
					$v = _makefilename( Request::file( $k )->getClientOriginalName() );
					
					$is_upload = Request::file( $k )->move( _filepath( $file_keys[$k] ), $v );
					if( $is_upload ){
						if( isset( $Request_Data['v_old_files'][$k] ) ){
							@unlink( _filepath( $file_keys[$k] ).'/'.$Request_Data['v_old_files'][$k] );
						}
					}
				}
				$check = GeneralSettings::where( 'v_key', '=', $k )->get();
				if( !$check->count() ){
					GeneralSettings::create( array( 'v_key' => $k, 'l_value' => $v ) );
				}
				else{
					$row = GeneralSettings::find( $check[0]->id );
					$row->l_value = $v;
					$row->save();
				}
			}
			
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else{
			$pass_array = array(
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data' => _get_settings(),
				'_admin_languages' => _get_admin_langs(),
				'_site_languages' => Language::getDropDownArray(),
			);
			//_p($this->_section_info['_view']);exit;
			return view( $this->_section_info['_view'], $pass_array );
		}
		
	}
	
	
}
