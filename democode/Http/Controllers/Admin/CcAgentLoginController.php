<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\Admin;
use App\Http\Requests\Admin\LoginRequest;

class CcAgentLoginController extends Controller {
	
	protected $_section_key, $_section_info;
	
	public function __construct(){
		//_p( Session::flush() );
		//_p( Session::all() );
		//unset( $_COOKIE['remember_user_59ba36addc2b2f9401580f014c7f58ea4e30989d'] );
		//unset( $_COOKIE['laravel_session'] );
		//foreach( $_COOKIE as $k => $v ){
			// unset( $_COOKIE[ $k ] );
		//}
		//_p( $_COOKIE );
		//exit;
		// auth()->guard('admin')->logout();
		// echo 'D - '.( auth()->guard('admin')->check() ? 'Y' : 'N' ); exit;
		
		$this->_section_key = '__LOGIN';
		$this->_section_info = _admin_sections( $this->_section_key );
		$this->_role = 'cc_admin';
		
	}

	public function showForm() {
		if( auth()->guard('cc_admin')->check() ){
			return redirect( _admin_sections( '__MANAGE_USERS', '_key' ) );
		}
		return view( $this->_section_info['_view'], array(
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_role' => $this->_role,
		) );
	}

	/**
	 * Login ->
	 * Authenticate User
	 */
	protected function login( LoginRequest $request ) {
		$remember_me = false;
		
		$auth = auth()->guard('cc_admin');
		if( $auth->attempt( $request->only('v_email', 'password'), $remember_me ) ){
			if( auth()->guard('cc_admin')->user()->e_status == 'inactive' ){
				auth()->guard('cc_admin')->logout();
				return redirect( $this->_section_info['_view'] )->with( 'msg', '0:account_inactive' );
			}
			return redirect( _admin_sections( '__MANAGE_USERS', '_key' ) );
		}
		else {
			$this->_section_info['_view'] = 'cc_admin/login';
			return redirect( $this->_section_info['_view'] )->with( 'msg', '0:invalid_login' );
		}
	}

	/**
	 * Logout
	 */
	protected function logout() {
		
		/*
		$loginData = LoginDetailsModel::where('e_type','admin')->where('i_login_id', auth()->guard('admin')->user()->id)->orderBy('id','DESC')->first();
		if( isset($loginData) && count($loginData) ) {
			LoginDetailsModel::find($loginData->id)->update([
				'd_logout'		=>	date('Y-m-d H:i:s'),
				'v_logout_ip' =>	Request::ip(),
			]);
		}
		*/
		$this->_section_info['_view'] = 'cc_admin/login';
		
		auth()->guard('cc_admin')->logout();
		
		return redirect( $this->_section_info['_view'] )->with( 'msg', '1:logout' );;
		
	}

	
}