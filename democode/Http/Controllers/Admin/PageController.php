<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\Page,
	App\Models\PageLang;

class PageController extends Controller{
	
	protected $_section_key, $_section_info;
	public function __construct(){
		
		$this->_section_key 	= '__MANAGE_PAGES';
		$this->_section_info 	= _admin_sections( $this->_section_key );
	}
	
	public function index(){
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_data'			=> Page::ApplySearch( array(
				'search' 	=> 'v_title,v_slug',
				'order' 	=> 'v_title,ASC',
			) ),
		);
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	
	public function action( $action, $id = '' ){
		$_lang_arr = _get_admin_langs();
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			
			$check = Page::where( 'v_slug', '=', $v_slug )->get();
			if( $check->count() ){
				if( $action == 'add' ){
					return redirect( $this->_section_info['_key'].'/add' )->with( 'msg', '0:exists_slug' );
				}
				else if( $action == 'edit' && $check[0]->id != $id ){
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '0:exists_slug' );
				}
			}
			
			if( $submit_btn == 'Submit' ){
				$row = Page::create( $Request_Data );
				$id = $row->id;
			}
			else if( $submit_btn == 'Update' ){
				$row = Page::find( $id );
				$row->update( $Request_Data );
			}
			
			
			foreach( $_lang_arr as $_lang => $_lang_title ){
				$langData = $Request_Data[$_lang];
				$file_keys = array(
					// 'v_image' => $this->_section_info['folder'],
				);
				foreach( $file_keys as $file => $path ){
					$imgKey = $file;
					$file = $file.'_'.$_lang;
					if( !isset( $Request_Data[ $file ] ) ){ continue; }
					$filename = $_lang.'-'._makefilename( Request::file( $file )->getClientOriginalName() );
					$is_upload = Request::file( $file )->move( _filepath( $path ), $filename );
					if( $is_upload ){
						if( isset( $langData['v_old_files'][$file] ) ){
							@unlink( _filepath( $file_keys[$file] ).'/'.$langData['v_old_files'][$file] );
						}
						$langData[$imgKey] = $filename;
					}
				}
				
				$langData['i_row_id'] = $id;
				if( $langData['id'] ){
					$rowLang = PageLang::find( $langData['id'] );
					$rowLang->update( $langData );
				}
				else{
					PageLang::create( $langData );
				}
			}
			
			
			if( $submit_btn == 'Submit' ){
				return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
			}
			else if( $submit_btn == 'Update' ){
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
			}
		}
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = Page::find( $id );
			$row->e_status = $action;
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			$row = Page::find( $id );
			
			$langData = $row->langData();
			foreach( $langData as $langRow ){
				$row2 = PageLang::find( $langRow['id'] );
				$row2->delete();
			}
			
			$row->delete();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			
			
			$data = array();
			$_lang_data = array();
			if( $action == 'edit' ){
				$data = Page::find( $id );
				$_lang_data = $data->langData();
			}
			
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
				'_lang_data'	=> $_lang_data,
				'_lang_arr'		=> _get_admin_langs(),
			);
			
			return view( $this->_section_info['_view'], $pass_array );
		}
		
	}
	
	
	
}
