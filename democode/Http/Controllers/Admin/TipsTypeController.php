<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\TipsType;
use App\Models\TipsTypeLang;

class TipsTypeController extends Controller{
	
	protected $_section_key, $_section_info;
	
	public function __construct(){
		$this->_section_key = '__MANAGE_TIPS_TYPES';
		$this->_section_info = _admin_sections( $this->_section_key );
	}
	
	public function index(){
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_data'			=> TipsType::ApplySearch( array(
				'search' 	=> 'v_title',
				'order' 	=> 'i_order,ASC',
			) )
		);
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	
	public function action( $action, $id = '' ){
		
		$_lang_arr = _get_admin_langs();
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			$check = TipsType::where( 'v_slug', '=', $v_slug )->get();
			if( $check->count() ){
				if( $action == 'add' ){
					return redirect( $this->_section_info['_key'].'/add' )->with( 'msg', '0:exists_slug' );
				}
				else if( $action == 'edit' && $check[0]->id != $id ){
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '0:exists_slug' );
				}
			}
			
			if( $submit_btn == 'Submit' ){
				$row = TipsType::create( $Request_Data );
				$id = $row->id;
			}
			else if( $submit_btn == 'Update' ){
				$row = TipsType::find( $id );
				$row->update( $Request_Data );
			}
			
			foreach( $_lang_arr as $_lang => $_lang_title ){
				$langData = $Request_Data[$_lang];
				
				$langData['i_row_id'] = $id;
				if( $langData['id'] ){
					$rowLang = TipsTypeLang::find( $langData['id'] );
					$rowLang->update( $langData );
				}
				else{
					TipsTypeLang::create( $langData );
				}
			}
			
			if( $submit_btn == 'Submit' ){
				return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
			}
			else if( $submit_btn == 'Update' ){
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
			}
		}
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = TipsType::find( $id );
			$row->e_status = $action;
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			$row = TipsType::find( $id );
			
			$langData = $row->langData();
			foreach( $langData as $langRow ){
				$row2 = TipsTypeLang::find( $langRow['id'] );
				$row2->delete();
			}
			
			$row->delete();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			
			$data = array();
			$_lang_data = array();
			if( $action == 'edit' ){
				$data = TipsType::find( $id );
				$_lang_data = $data->langData();
			}
			
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
				'_lang_data'	=> $_lang_data,
				'_lang_arr'		=> _get_admin_langs(),
			);
			return view( $this->_section_info['_view'], $pass_array );
		}
		
	}
	
}
