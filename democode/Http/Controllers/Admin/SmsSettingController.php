<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\GeneralSettings;

class SmsSettingController extends Controller{
	
	protected $_section_key, $_section_info;
	public function __construct(){
		
		$this->_section_key = '__MANAGE_SMS_SETTINGS';
		$this->_section_info = _admin_sections( $this->_section_key );
	}
	
	public function index(){
		
		$Request_Data = Request::all();
		
		$_lang_arr = _get_admin_langs();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] == 'Test Send' ){
			
			$test_num = trim( _putval( $Request_Data, 'test_num' ) );
			if( !$test_num ){
				return redirect( $this->_section_info['_key'] )->with( 'msg', '0:sms_invalid' );
			}
			else{
				
				$test_sms = _putval( $Request_Data, 'test_sms', "Testing SMS From IDProtect" );
				
				$sms = _send_sms( array(
					'_to' => $test_num,
					'_body' => $test_sms,
					'_keywords' => array(
					),
				) );
				
				if( $sms ){
					return redirect( $this->_section_info['_key'] )->with( 'msg', array( '1:sms_sent', 'info1:'.$sms ) );
				}
				else{
					return redirect( $this->_section_info['_key'] )->with( 'msg', array( '0:sms_not_sent', 'info0:'.$sms ) );
				}
			}
			
		}
		
		else if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] == 'Update' ){
			
			unset( $Request_Data['_token'] );
			unset( $Request_Data['submit_btn'] );
			$avoid_keys = array( '_token', 'submit_btn' );
			
			foreach( $Request_Data as $k => $v ){
				if( in_array( $k, $avoid_keys ) ) continue;
				$check = GeneralSettings::where( 'v_key', '=', $k )->get();
				if( !$check->count() ){
					GeneralSettings::create( array( 'v_key' => $k, 'l_value' => $v ) );
				}
				else{
					$row = GeneralSettings::find( $check[0]->id );
					$row->l_value = $v;
					$row->save();
				}
			}
			
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
			
		}
		
		else{
			$pass_array = array(
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data' 		=> _get_settings(),
				'_lang_arr' 	=> $_lang_arr,
			);
			return view( $this->_section_info['_view'], $pass_array );
			
		}
		
	}
	
	
}
