<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\CcAgent;

class CcAgentController extends Controller{
	
	protected $_section_key, $_section_info;
	
	public function __construct(){
		$this->_section_key 	= '__MANAGE_CC_USERS';
		$this->_section_info 	= _admin_sections( $this->_section_key );
	}
	
	public function index(){
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_data'			=> CcAgent::ApplySearch( array(
				'search' 	=> 'v_name,v_email',
				'order' 	=> 'v_name,ASC',
			) ),
		);
		
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	public function action( $action, $id = '' ){
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			// _p( Hash::make( $Request_Data['password'] ) );
			// _p( bcrypt( $Request_Data['password'] ) );
			
			if( $submit_btn == 'Submit' ){
				$check = CcAgent::where( 'v_email', '=', $v_email )->get();
				if( !$check->count() ){
					$Request_Data['password'] = Hash::make( $Request_Data['password'] );
					CcAgent::create( $Request_Data );
					return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
				}
				else{
					return redirect( $this->_section_info['_key'].'/add' )->with( 'msg', '0:exists_email' );
				}
			}
			else if( $submit_btn == 'Update' ){
				$check = CcAgent::where( 'id', '!=', $id )->where( 'v_email', '=', $v_email )->get();
				if( !$check->count() ){
					$row = CcAgent::find( $id );
					
					if( trim( $Request_Data['password'] ) ){
						$Request_Data['password'] = Hash::make( $Request_Data['password'] );
					}
					else{
						unset( $Request_Data['password'] );
					}
					$row->update( $Request_Data );
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
				}
				else{
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '0:exists_email' );
				}
			}
		}
		
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = CcAgent::find( $id );
			$row->e_status = $action;
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			CcAgent::find( $id )->delete();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			
			//$row = DB::select( "SHOW COLUMNS FROM tbl_users;" );
			//$row = json_decode( json_encode( $row ), true );
			//_p( $row ); exit;
			
			$data = array();
			if( $action == 'edit' ){
				$data = CcAgent::find( $id );
			}
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
				'_test'			=> 0,
			);
			return view( $this->_section_info['_view'], $pass_array );
		}
		
	}
	
}
