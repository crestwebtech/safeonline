<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Admin;
use App\Http\Requests\Admin\RegisterRequest;

class RegisterController extends Controller {
	
	protected $_section_key, $_section_info;
	
	public function __construct(){
		if( auth()->guard('admin')->check() ){
			return redirect('admin/general-settings');
		}
		$this->_section_key = '__REGISTER';
		$this->_section_info = _admin_sections( $this->_section_key );
	}
	
	public function showForm() {
		return view( $this->_section_info['_view'], array(
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
		) );
	}


	/**
	 * Register
	 */
	protected function register( RegisterRequest $request ){
		$data = $request->all();
		$data['password'] = Hash::make($data['password']);
		Admin::create( $data );
		
		$temp = _admin_sections( '__LOGIN' );
		
		return redirect( $temp['_view'] )->with( 'msg', '1:register' );
		
	}
	
}