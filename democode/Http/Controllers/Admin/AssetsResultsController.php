<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\AssetsResults;

use App\FrontUser;
use Request, Session, DB;

class AssetsResultsController extends Controller{
	
	protected $_section_key, $_section_info;
	
	public function __construct(){
		$this->_section_key 	= '__MANAGE_USERS_ASSETS_RESULTS';
		$this->_section_info 	= _admin_sections( $this->_section_key );
	}
	
	public function index(){
		
		$_users = array(
			'' => '- Select User -',
		);
		
		$temp = FrontUser::orderBy( 'v_user_id', 'ASC' )->get()->toArray();
		if( count( $temp ) ){
			foreach( $temp as $row ){
				$_users[$row['id']] = $row['v_user_id']." - ".trim( $row['v_first_name']." ".$row['v_surname'] );
			}
		}
		
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_users' 		=> $_users,
			'_data'			=> AssetsResults::ApplySearch( array(
				'search' 	=> 'i_user_id,v_user_id,v_result_id,v_result_category,v_result_title,v_result_source,v_result_risk,v_result,d_scan_date',
				'order' 	=> 'd_scan_date,DESC',
				'type' 		=> 'assets_results',
			) )
		);
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	
	public function indexCcAgent(){
		
		$_users = array(
			'' => '- Select User -',
		);
		
		$temp = FrontUser::orderBy( 'v_tcs_id', 'ASC' )->get()->toArray();
		if( count( $temp ) ){
			foreach( $temp as $row ){
				$_users[$row['id']] = $row['v_tcs_id']." - ".trim( $row['v_first_name']." ".$row['v_surname'] );
			}
		}
		
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_users' 		=> $_users,
			'_data'			=> AssetsResults::ApplySearch( array(
				'search' 	=> 'i_user_id,v_user_id,v_result_id,v_result_category,v_result_title,v_result_source,v_result_risk,v_result,d_scan_date',
				'order' 	=> 'd_scan_date,DESC',
				'type' 		=> 'cc_admin_assets_results',
			) )
		);
		
		return view( 'admin/ccagent-users-assets-results', $pass_array );
	}
	
	
	public function action( $action, $id = '' ){
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			if( $submit_btn == 'Submit' ){
				$check = AssetsResults::where( 'v_email', '=', $v_email )->get();
				if( !$check->count() ){
					AssetsResults::create( $Request_Data );
					return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
				}
				else{
					return redirect( $this->_section_info['_key'].'/add' )->with( 'msg', '0:exists_email' );
				}
			}
			else if( $submit_btn == 'Update' ){
				$check = AssetsResults::where( 'id', '!=', $id )->where( 'v_email', '=', $v_email )->get();
				if( !$check->count() ){
					$row = AssetsResults::find( $id );
					$row->update( $Request_Data );
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
				}
				else{
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '0:exists_email' );
				}
				
			}
			
		}
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = AssetsResults::find( $id );
			$row->e_status = $action;
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			AssetsResults::find( $id )->delete();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			$data = array();
			if( $action == 'edit' ){
				$data = AssetsResults::find( $id );
			}
			
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
			);
			return view( $this->_section_info['_view'], $pass_array );
		}
		
	}
	
	public function actionCcAgent( $action, $id = '' ){
		
		$Request_Data = Request::all();
		
		if( $action == 'remove' ){
			
			$row = AssetsResults::find( $id );
			
			$user = FrontUser::find( $row->i_user_id );
			
			## SENT EMAIL
			$mail = _send_mail( array(
				'_to' => $user->v_email,
				'_to_name' => $user->v_first_name." ".$user->v_surname,
				'_key' => 'customer_result_delete_by_ccagent',
				'_lang' => $user->v_lang,
				'_keywords' => array(
					'[user_firstname]' => $user->v_first_name,
					'[user_surname]' => $user->v_surname,
					'[result_id]' => $row->v_result_id,
				),
			) );
			AssetsResults::find( $id )->delete();
			
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		
	}
	
	
	
}
