<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\Language;

class LanguageController extends Controller{
	
	protected $_section_key, $_section_info;
	
	public function __construct(){
		
		$this->_section_key = '__MANAGE_LANGUAGE';
		$this->_section_info = _admin_sections( $this->_section_key );
		$this->_section_info['folder'] = 'language';
	}
	
	public function index(){
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_data'			=> Language::ApplySearch( array(
				'search' 	=> 'v_title,v_key',
				'order' 	=> 'i_order,ASC',
			) ),
		);
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	
	public function action( $action, $id = '' ){
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			$file_keys = array(
				'v_image' 	=> $this->_section_info['folder'],
			);
			foreach( $file_keys as $file => $path ){
				if( !isset( $Request_Data[ $file ] ) ){ continue; }
				$filename = _makefilename( Request::file( $file )->getClientOriginalName() );
				$is_upload = Request::file( $file )->move( _filepath( $path ), $filename );
				if( $is_upload ){
					if( isset( $Request_Data['v_old_files'][$file] ) ){
						@unlink( _filepath( $file_keys[ $file ] ).'/'.$Request_Data['v_old_files'][ $file ] );
					}
					$Request_Data[$file] = $filename;
				}
			}
			
			if( $submit_btn == 'Submit' ){
				$row = Language::create( $Request_Data );
			}
			else if( $submit_btn == 'Update' ){
				$row = Language::find( $id );
				$row->update( $Request_Data );
			}
			if( $submit_btn == 'Submit' ){
				return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
			}
			else if( $submit_btn == 'Update' ){
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
			}
		}
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = Language::find( $id );
			$row->e_status = $action;
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			//$row = Language::find( $id );
			//@unlink( _filepath( 'homepage-slider/'.$row->v_image ) );
			//$row->delete();
			//return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			
			$data = array();
			if( $action == 'edit' ){
				$data = Language::find( $id );
			}
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
			);
			return view( $this->_section_info['_view'], $pass_array );
		}
		
	}
	
}
