<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\Faq,
	App\Models\FaqLang;
use App\Models\FaqType;

class FaqController extends Controller{
	
	protected $_section_key, $_section_info;
	public function __construct(){
		
		$this->_section_key 	= '__MANAGE_FAQS';
		$this->_section_info 	= _admin_sections( $this->_section_key );
		
		// $this->_section_info['folder'] = '';
		
		$this->_images = array(
			// 'v_image' => $this->_section_info['folder'],
		);
		
		$_faq_type = array(
			'' => '- Select -',
		);
		$temp = FaqType::all()->sortBy('i_order');
		if( $temp->count() ){
			foreach( $temp as $row ){
				$_faq_type[$row->id] = $row->v_title;
			}
		}
		
		$this->_faq_type = $_faq_type;
	}
	
	
	public function index(){
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_faq_type' 	=> $this->_faq_type,
			'_data'			=> Faq::ApplySearch( array(
				'search' 	=> 'v_title',
				'order' 	=> 'i_order,ASC',
			) )
		);
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	
	public function action( $action, $id = '' ){
		
		$_lang_arr = _get_admin_langs();
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			// _p( $Request_Data ); exit;
			
			if( $submit_btn == 'Submit' ){
				$row = Faq::create( $Request_Data );
				$id = $row->id;
			}
			else if( $submit_btn == 'Update' ){
				$row = Faq::find( $id );
				$row->update( $Request_Data );
			}
			
			foreach( $_lang_arr as $_lang => $_lang_title ){
				$langData = $Request_Data[$_lang];
				$file_keys = $this->_images;
				foreach( $file_keys as $file => $path ){
					$imgKey = $file;
					$file = $file.'_'.$_lang;
					if( !isset( $Request_Data[ $file ] ) ){ continue; }
					$filename = $_lang.'-'._makefilename( Request::file( $file )->getClientOriginalName() );
					$is_upload = Request::file( $file )->move( _filepath( $path ), $filename );
					if( $is_upload ){
						if( isset( $langData['v_old_files'][$file] ) ){
							@unlink( _filepath( $file_keys[$file] ).'/'.$langData['v_old_files'][$file] );
						}
						$langData[$imgKey] = $filename;
					}
				}
				$langData['i_row_id'] = $id;
				if( $langData['id'] ){
					$rowLang = FaqLang::find( $langData['id'] );
					$rowLang->update( $langData );
				}
				else{
					FaqLang::create( $langData );
				}
			}
			
			if( $submit_btn == 'Submit' ){
				return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
			}
			else if( $submit_btn == 'Update' ){
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
			}
		}
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = Faq::find( $id );
			$row->e_status = $action;
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			$row = Faq::find( $id );
			@unlink( _filepath( $this->_section_info['folder'].'/'.$row->v_image ) );
			
			$langData = $row->langData();
			foreach( $langData as $langRow ){
				$row2 = FaqLang::find( $langRow['id'] );
				@unlink( _filepath( $this->_section_info['folder'].'/'.$row2['v_image'] ) );
				$row2->delete();
			}
			
			$row->delete();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			
			$data = array();
			$_lang_data = array();
			if( $action == 'edit' ){
				$data = Faq::find( $id );
				$_lang_data = $data->langData();
			}
			
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
				'_faq_type' 	=> $this->_faq_type,
				'_lang_data'	=> $_lang_data,
				'_lang_arr'		=> _get_admin_langs(),
			);
			return view( $this->_section_info['_view'], $pass_array );
		}
		
	}
	
	
	
}
