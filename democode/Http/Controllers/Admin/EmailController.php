<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\Email,
	App\Models\EmailLang;

class EmailController extends Controller{
	
	protected $_section_key, $_section_info;
	public function __construct(){
		
		$this->_section_key 	= '__MANAGE_EMAIL';
		$this->_section_info 	= _admin_sections( $this->_section_key );
		
		$this->_mail_keys 		= array(
			'' 									=> '- Select -',
			
			'admin_deletion_request' 			=> 'Admin Deletion Sentrybay Request',
			
			'customer_register' 				=> 'Customer Register',
			'customer_forgot_password' 			=> 'Customer Forgot Password',
			'customer_sentrybay_notification' 	=> 'Customer Notification of Sentrybay Result',
			
			
			'customer_password_reset_by_ccagent'=> 'Customer Password Reset By CC Agent',
			'customer_result_delete_by_ccagent' => 'Customer Result Delete By CC Agent',
			
		);
		
		$this->replaceKeywords = _keywords();
		
	}
	
	
	
	public function index(){
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_data'			=> Email::ApplySearch( array(
				'search' 	=> 'v_subject,v_key',
				'order' 	=> 'v_subject,ASC',
			) )
		);
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	
	public function action( $action, $id = '' ){
		
		$_lang_arr = _get_admin_langs();
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			if( $submit_btn == 'Submit' ){
				$row = Email::create( $Request_Data );
				$id = $row->id;
			}
			else if( $submit_btn == 'Update' ){
				$row = Email::find( $id );
				$row->update( $Request_Data );
			}
			
			foreach( $_lang_arr as $_lang => $_lang_title ){
				$langData = $Request_Data[$_lang];
				$file_keys = array(
					// 'v_image' => $this->_section_info['folder'],
				);
				foreach( $file_keys as $file => $path ){
					$imgKey = $file;
					$file = $file.'_'.$_lang;
					if( !isset( $Request_Data[ $file ] ) ){ continue; }
					$filename = $_lang.'-'._makefilename( Request::file( $file )->getClientOriginalName() );
					$is_upload = Request::file( $file )->move( _filepath( $path ), $filename );
					if( $is_upload ){
						if( isset( $langData['v_old_files'][$file] ) ){
							@unlink( _filepath( $file_keys[$file] ).'/'.$langData['v_old_files'][$file] );
						}
						$langData[$imgKey] = $filename;
					}
				}
				
				$langData['i_row_id'] = $id;
				if( $langData['id'] ){
					$rowLang = EmailLang::find( $langData['id'] );
					$rowLang->update( $langData );
				}
				else{
					EmailLang::create( $langData );
				}
			}
			
			if( $submit_btn == 'Submit' ){
				return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
			}
			else if( $submit_btn == 'Update' ){
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
			}
		}
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = Email::find( $id );
			$row->e_status = $action;
			
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			$row = Email::find( $id );
			// @unlink( _filepath( $this->_section_info['folder'].'/'.$row->v_image ) );
			$langData = $row->langData();
			foreach( $langData as $langRow ){
				$row2 = EmailLang::find( $langRow['id'] );
				// @unlink( _filepath( $this->_section_info['folder'].'/'.$row2['v_image'] ) );
				$row2->delete();
			}
			
			$row->delete();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			
			$data = array();
			$_lang_data = array();
			if( $action == 'edit' ){
				$data = Email::find( $id );
				$_lang_data = $data->langData();
			}
			// _p( $data );
			
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_mail_keys' 	=> $this->_mail_keys,
				'_data'			=> $data,
				'_lang_data'	=> $_lang_data,
				'_lang_arr'		=> _get_admin_langs(),
				'_keywords'		=> $this->replaceKeywords,
			);
			return view( $this->_section_info['_view'], $pass_array );
		}
	}
	
	
	
}
