<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\Translation;

class TranslationController extends Controller{
	
	protected $_section_key, $_section_info;
	public function __construct(){
		
		$this->_section_key 	= '__MANAGE_TRANSLATION';
		
		$this->_section_info 	= _admin_sections( $this->_section_key );
		
		$this->_section_info['folder'] = "translation";
		
	}
	
	
	
	public function index(){
		
		$_lang_arr = _get_admin_langs();
		
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_data'			=> Translation::ApplySearch( array(
				'search' 	=> 'v_title,v_key',
				'order' 	=> 'v_title,ASC',
			) ),
			'_lang_arr'		=> $_lang_arr,
			'_lang_data'	=> array(),
		);
		
		foreach( $_lang_arr as $_lang => $_lang_title ){
			$pass_array['_lang_data'][$_lang] = _all_translactions( $_lang );
		}
		
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	
	public function action( $action, $id = '' ){
		
		$_lang_arr = _get_admin_langs();
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			if( $submit_btn == 'Submit' ){
				$row = Translation::where( "v_key", "=", $Request_Data["v_key"] )->get();
				if( $row->count( $row ) ){
					return redirect( $this->_section_info['_key'].'/add' )->with( 'msg', '0:exists_key' );
				}
				else{
					$row = Translation::create( $Request_Data );
					$id = $row->id;
				}
			}
			else if( $submit_btn == 'Update' ){
				
				$row = Translation::where( "v_key", "=", $Request_Data["v_key"] )
					->where( "id", "!=", $id )->get();
				if( $row->count( $row ) ){
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '0:exists_key' );
				}
				else{
					$row = Translation::find( $id );
					$row->update( $Request_Data );	
				}
			}
			
			foreach( $_lang_arr as $_lang => $_lang_title ){
				
				$fileName = $_lang.'.json';
				
				if( !Storage::disk('language-translation')->exists( $fileName ) ){
					Storage::disk('language-translation')->put( $fileName, json_encode( array(
						'_dummy' => 'Dummy'
					) ) );
				}
				
				$fileData = Storage::disk('language-translation')->get( $fileName );
				if( $fileData ){
					$fileData = json_decode( $fileData, true );
					$fileData[ $_REQUEST['v_key'] ] = $Request_Data['l_description'][$_lang];
					ksort( $fileData );
				}
				
				Storage::disk('language-translation')->put( $fileName, json_encode( $fileData ) );
				
			}
			
			
			if( $submit_btn == 'Submit' ){
				return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
			}
			else if( $submit_btn == 'Update' ){
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
			}
		}
		
		else if( $action == 'remove' ){
			
			$row = Translation::find( $id );
			$langData = Translation::langData( $row->v_key );
			
			foreach( $_lang_arr as $_lang => $_lang_title ){
				if( !isset( $langData[$_lang] ) ){
					continue;
				}
				$fileName = $_lang.'.json';
				
				$fileData = Storage::disk('language-translation')->get( $fileName );
				if( $fileData ){
					$fileData = json_decode( $fileData, true );
					$fileData[ $row->v_key ] = $fileData[ $row->v_key ];
					unset( $fileData[ $row->v_key ] );
					ksort( $fileData );
				}
				Storage::disk('language-translation')->put( $fileName, json_encode( $fileData ) );
				
			}
			$row->delete();
			
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			
			$data = array();
			$_lang_data = array();
			if( $action == 'edit' ){
				$data = Translation::find( $id );
				$_lang_data = Translation::langData( $data->v_key );
			}
			
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
				'_lang_data'	=> $_lang_data,
				'_lang_arr'		=> $_lang_arr,
			);
			return view( $this->_section_info['_view'], $pass_array );
		}
	}
	
	
	
}
