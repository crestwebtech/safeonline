<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\Tips,
	App\Models\TipsLang;
use App\Models\TipsType;
use App\Models\TipsGroup;

class TipsController extends Controller{
	
	protected $_section_key, $_section_info;
	public function __construct(){
		
		$this->_section_key 	= '__MANAGE_TIPS';
		$this->_section_info 	= _admin_sections( $this->_section_key );
		
		$_tips_type = array(
			'' => '- Tips Type -',
		);
		$temp = TipsType::all()->sortBy('i_order');
		if( $temp->count() ){
			foreach( $temp as $row ){
				$_tips_type[$row->id] = $row->v_title;
			}
		}
		
		$this->_tips_type = $_tips_type;
		
		
		$_tips_group = array(
			'' => '- Tips Group -',
		);
		if( $srch_tips_type = _putval( $_GET, 'srch_tips_type' ) ){
			$temp = TipsGroup::where( "i_tips_type_id", "=", $srch_tips_type )->orderBy('i_order')->get();
		}
		else{
			$temp = TipsGroup::all()->sortBy('i_order');
		}
		
		if( $temp->count() ){
			foreach( $temp as $row ){
				$_tips_group[$row->id] = $row->v_title;
			}
		}
		
		$this->_tips_group = $_tips_group;
	}
	
	
	public function index(){
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_tips_type' 	=> $this->_tips_type,
			'_tips_group' 	=> $this->_tips_group,
			'_data'			=> Tips::ApplySearch( array(
				'search' 	=> 'tbl_tips.v_title',
				'order' 	=> 'i_order,ASC',
				'type' 		=> 'tips',
			) )
		);
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	
	public function action( $action, $id = '' ){
		
		$_lang_arr = _get_admin_langs();
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			// _p( $Request_Data ); exit;
			
			if( $submit_btn == 'Submit' ){
				$row = Tips::create( $Request_Data );
				$id = $row->id;
			}
			else if( $submit_btn == 'Update' ){
				$row = Tips::find( $id );
				$row->update( $Request_Data );
			}
			
			foreach( $_lang_arr as $_lang => $_lang_title ){
				$langData = $Request_Data[$_lang];
				
				$langData['i_row_id'] = $id;
				if( $langData['id'] ){
					$rowLang = TipsLang::find( $langData['id'] );
					$rowLang->update( $langData );
				}
				else{
					TipsLang::create( $langData );
				}
			}
			
			if( $submit_btn == 'Submit' ){
				return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
			}
			else if( $submit_btn == 'Update' ){
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
			}
		}
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = Tips::find( $id );
			$row->e_status = $action;
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			$row = Tips::find( $id );
			@unlink( _filepath( $this->_section_info['folder'].'/'.$row->v_image ) );
			
			$langData = $row->langData();
			foreach( $langData as $langRow ){
				$row2 = TipsLang::find( $langRow['id'] );
				@unlink( _filepath( $this->_section_info['folder'].'/'.$row2['v_image'] ) );
				$row2->delete();
			}
			
			$row->delete();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			
			$data = array();
			$_lang_data = array();
			if( $action == 'edit' ){
				$data = Tips::find( $id );
				$_lang_data = $data->langData();
			}
			
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
				'_tips_type' 	=> $this->_tips_type,
				'_lang_data'	=> $_lang_data,
				'_lang_arr'		=> _get_admin_langs(),
			);
			return view( $this->_section_info['_view'], $pass_array );
		}
		
	}
	
	
	
}
