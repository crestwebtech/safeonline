<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\GeneralSettings;
use App\Models\Faq;
use App\Models\TipsGroup;

class AjaxController extends Controller{
	
	public function loadParentFaq(){
		
		$Request_Data = Request::all();
		
		$_faq_type = array(
			'0' => '- Parent -',
		);
		$temp = Faq::where( 'i_faq_type_id', '=', $_REQUEST['faq_type_id'] )->where( 'i_parent_id', '=', '0' )->orderBy('i_order')->get();
		if( $temp->count() ){
			foreach( $temp as $row ){
				$_faq_type[$row->id] = $row->v_title;
			}
		}
		$_REQUEST['selectedval'] = isset( $_REQUEST['selectedval'] ) ? $_REQUEST['selectedval'] : 0;
		echo _keyval_dropdown( $_faq_type, $_REQUEST['selectedval'] ); exit;
		
	}
	
	
	public function loadTipsGroups(){
		$RequestData = Request::all();
		$arr = array(
			'' => '- Tips Groups -',
		);
		$temp = TipsGroup::where( 'i_tips_type_id', '=', _putval( $RequestData, 'id', 0 ) )->orderBy('i_order')->get();
		if( $temp->count() ){
			foreach( $temp as $row ){
				$arr[ $row->id ] = $row->v_title;
			}
		}
		echo _keyval_dropdown( $arr, _putval( $RequestData, 'selected', 0 ) ); exit;
	}
	
}
