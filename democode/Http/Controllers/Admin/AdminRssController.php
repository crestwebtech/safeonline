<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Request, Response, Session, Auth, DB, File, Storage, Hash, Validator, Carbon\Carbon;

use App\Models\RSS;

class AdminRssController extends Controller{
	
	protected $_section_key, $_section_info;
	
	public function __construct(){
		$this->_section_key 	= '__MANAGE_RSS_FEEDS';
		$this->_section_info 	= _admin_sections( $this->_section_key );
	}
	
	public function index(){
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_data'			=> RSS::ApplySearch( array(
				'search' 	=> 'v_title,l_description',
				'order' 	=> 'd_added,DESC',
			) ),
		);
		return view( $this->_section_info['_view'], $pass_array );
	}
	
	public function action( $action, $id = '' ){
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			
			if( $submit_btn == 'Submit' ){
				$check = RSS::where( 'v_email', '=', $v_email )->get();
				if( !$check->count() ){
					$Request_Data['password'] = Hash::make( $Request_Data['password'] );
					RSS::create( $Request_Data );
					return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
				}
				else{
					return redirect( $this->_section_info['_key'].'/add' )->with( 'msg', '0:exists_email' );
				}
			}
			else if( $submit_btn == 'Update' ){
				$check = RSS::where( 'id', '!=', $id )->where( 'v_email', '=', $v_email )->get();
				if( !$check->count() ){
					$row = RSS::find( $id );
					if( trim( $Request_Data['password'] ) ){
						$Request_Data['password'] = Hash::make( $Request_Data['password'] );
					}
					$row->update( $Request_Data );
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
				}
				else{
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '0:exists_email' );
				}
			}
		}
		
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = RSS::find( $id );
			$row->e_status = $action;
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			RSS::find( $id )->delete();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			$data = array();
			if( $action == 'edit' ){
				$data = RSS::find( $id );
			}
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
			);
			return view( $this->_section_info['_view'], $pass_array );
		}
		
	}
	
}
