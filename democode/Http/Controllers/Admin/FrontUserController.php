<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;



use App\FrontUser;
use Request, Session, DB, Hash;


class FrontUserController extends Controller{
	
	protected $_section_key, $_section_info;
	public function __construct(){
		
		$this->_section_key 	= '__MANAGE_USERS';
		$this->_section_info 	= _admin_sections( $this->_section_key );
	}
	
	public function index(){
		## $projections = array( 'id', 'name' );
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_data'			=> FrontUser::ApplySearch( array(
				'search' 	=> 'v_user_id,v_tcs_id,v_first_name,v_surname,v_email,v_phone',
				'order' 	=> 'v_user_id,ASC',
			) )
		);
		return view( 'admin.users', $pass_array );
	}
	
	public function indexCcAgent(){
		$pass_array = array(
			'_SHOW_TYPE' 	=> 'list',
			'_section_key' 	=> $this->_section_key,
			'_section_info' => $this->_section_info,
			'_data'			=> FrontUser::ApplySearch( array(
				'search' 	=> 'v_tcs_id,v_first_name,v_surname',
				'order' 	=> 'v_tcs_id,ASC',
			) )
		);
		return view( 'admin.ccagent-users', $pass_array );
	}
	
	
	public function action( $action, $id = '' ){
		
		$Request_Data = Request::all();
		
		if( 0 && isset( $Request_Data['createUser'] ) && $Request_Data['createUser'] ){
			/*
			$array = array(
				'Philipp.hoyer@i-surance.ch',
				'Jens.schaedler@i-surance.ch',
				'Pascale.schaedler@i-surance.ch',
				'Anais.baltsavias@i-surance.de',
				'Elisa.darold@i-surance.de',
				'Elisa.simeoni@i-surance.de',
				'Franca.kruse@i-surance.de',
				'Giulia.salezze@i-surance.de',
				'Gustavo.sanchez@i-surance.de',
				'Helene.launay@i-surance.de',
				'Lisa.hofmann@i-surance.de',
				'Luana.Naquin@i-surance.de',
				'Margherita.mazzoni@i-surance.de',
				'Mariken.vangulpen@i-surance.de',
				'martina.deltetto@i-surance.de',
				'Paul.ackermann@i-surance.de',
				'Robert.horst@i-surance.de',
				'Sarah.hamon@i-surance.de',
				'Valerio.adragna@i-surance.de',
				'Zidane.momo@i-surance.de',
				'Jeanne.romero@i-surance.es',
				'Marion.rapin@i-surance.es',
				'Ruxandra.bogdan@i-surance.fr',
			);	
			foreach( $array as $v_email ){
				$check = FrontUser::where( 'v_email', '=', $v_email )->get();
				if( $check->count() ){
					$row = FrontUser::find( $check[0]->id );
					$row->password = Hash::make( 'I-surance_2017' );
					$row->save();
				}
				else{
					$row = FrontUser::create( array(
						'v_email' => $v_email,
					) );
					$row->v_user_id = str_pad( $row->id, 4, "1000", STR_PAD_LEFT );
					$row->password = Hash::make( 'I-surance_2017' );
					$row->save();
				}
			}
			_p( $array ); exit;
			*/
		}
		
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			$Request_Data['d_birthdate'] = _date( $Request_Data['d_birthdate'], 'db' );
			
			if( $submit_btn == 'Submit' ){
				$check = FrontUser::where( 'v_email', '=', $v_email )->get();
				if( !$check->count() ){
					
					$row = FrontUser::create( $Request_Data );
					$row->v_user_id = str_pad( $row->id, 4, "1000", STR_PAD_LEFT );
					
					if( $Request_Data['password'] ){
						$Request_Data['password'] = Hash::make( $password );
					}
					else{
						unset( $Request_Data['password'] );
					}
					
					$row->save();
					
					return redirect( $this->_section_info['_key'] )->with( 'msg', '1:added' );
				}
				else{
					return redirect( $this->_section_info['_key'].'/add' )->with( 'msg', '0:exists_email' );
				}
			}
			else if( $submit_btn == 'Update' ){
				$check = FrontUser::where( 'id', '!=', $id )->where( 'v_email', '=', $v_email )->get();
				if( !$check->count() ){
					$row = FrontUser::find( $id );
					if( $Request_Data['password'] ){
						$Request_Data['password'] = Hash::make( $password );
					}
					else{
						unset( $Request_Data['password'] );
					}
					$row->update( $Request_Data );
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
				}
				else{
					return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '0:exists_email' );
				}
				
			}
			
		}
		else if( in_array( $action, array( 'active', 'inactive' ) ) ){
			$row = FrontUser::find( $id );
			$row->e_status = $action;
			$row->save();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:updated' );
		}
		else if( $action == 'remove' ){
			FrontUser::find( $id )->delete();
			return redirect( $this->_section_info['_key'] )->with( 'msg', '1:deleted' );
		}
		else{
			$data = array();
			if( $action == 'edit' ){
				$data = FrontUser::find( $id );
			}
			
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
			);
			return view( 'admin.users', $pass_array );
		}
		
	}
	
	public function actionCcAgent( $action, $id = '' ){
		
		$Request_Data = Request::all();
		
		if( isset( $Request_Data['submit_btn'] ) && $Request_Data['submit_btn'] ){
			
			extract( $Request_Data );
			
			if( !trim( $Request_Data['password'] ) ){
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '0:required_password' );
			}
			
			$check = FrontUser::where( 'id', '=', $id )->get();
			if( $check->count() ){
				
				$row = FrontUser::find( $id );
				$Request_Data['password'] = Hash::make( $password );
				$row->update( $Request_Data );
				
				## SENT EMAIL
				$mail = _send_mail( array(
					'_to' => $row->v_email,
					'_to_name' => $row->v_first_name." ".$row->v_surname,
					'_key' => 'customer_password_reset_by_ccagent',
					'_lang' => $row->v_lang,
					'_keywords' => array(
						'[user_firstname]' => $row->v_first_name,
						'[user_surname]' => $row->v_surname,
						'[password]' => $password,
					),
				) );
				
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '1:updated' );
			}
			else{
				return redirect( $this->_section_info['_key'].'/edit/'.$id )->with( 'msg', '0:not_found' );
			}
		}
		
		else{
			$data = array();
			if( $action == 'edit' ){
				$data = FrontUser::find( $id );
			}
			$pass_array = array(
				'_SHOW_TYPE' 	=> $action,
				'_section_key' 	=> $this->_section_key,
				'_section_info' => $this->_section_info,
				'_data'			=> $data,
			);
			return view( 'admin.ccagent-users', $pass_array );
		}
		
	}
	
}
