<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\GeneralSettings;
use App\LanguageTranslation;
use App\Language;

use Request, Session, Auth, DB, File, Storage, Response;

class ApiController extends Controller{
	
	public function index( $action ){
		
		$Request_Data = Request::all();
		
		if( $action == 'get-label-translation' ){
			$_data = LanguageTranslation::all();
			$lang = isset( $Request_Data['lang'] ) ? $Request_Data['lang'] : 'en';
			if( $_data->count() ){
				$_data = $_data[0];
				if( isset( $_data['l_data'][$lang] ) ){
					return $_data['l_data'][$lang];
				}
				else{
					return $_data['l_data']['en'];
				}
			}
			return json_encode( array() );
		}
		
	}
	
	public function getLabelTranslation(){
		$Request_Data = Request::all();
		$lang = isset( $Request_Data['lang'] ) ? $Request_Data['lang'] : 'en';
		$_data = LanguageTranslation::all();
		if( $_data->count() ){
			$_data = $_data[0];
			if( isset( $_data['l_data'][$lang] ) ){
				return $_data['l_data'][$lang];
			}
			else{
				return $_data['l_data']['en'];
			}
		}
		return json_encode( array() );
	}
	
	
	
}
