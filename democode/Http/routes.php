<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//use App\User;
//use Illuminate\Http\Request;


use App\Session;

/*
Route::get('/clear-cache', function(){
	$exitCode = Artisan::call('cache:clear');
    // return what you want
});*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

## Crons
Route::group([ 'prefix' => 'cron/', 'middleware' => ['web', 'lang']], function (){
	
	## Testing
	Route::get( '/test', 'Crons\RssController@test' );
	
	Route::get( '/sleep1', 'Crons\RssController@sleep1' );
	Route::get( '/sleep2', 'Crons\RssController@sleep2' );
	
	## Reading RSS
	Route::get( '/rss-reader/{id}', 'Crons\RssController@index' );
	
	## SentryBay Extract Results
	Route::get( '/sentrybay/extract-result', 'Crons\SentrybayController@extractResult' );
	Route::get( '/sentrybay/extract-result-demo', 'Crons\SentrybayController@extractResultDemo' );
	
});

## Front - Free Access
Route::group([ 'prefix' => '/', 'middleware' => ['web', 'lang']], function () {
	
	Route::get( '/', 'Front\CommonController@homepage' );
	
	Route::get( '/faqs', 'Front\CommonController@faqs' );
	
	Route::get( '/pages/{_slug}', 'Front\CommonController@pages' );
	
	Route::get( '/sentrybay-demo/{type}', 'Front\CommonController@sentrybayDemo' );
	Route::get( '/sms-demo', 'Front\CommonController@smsDemo' );
	
});

## Front - Restricted Access
Route::group([ 'prefix' => '/', 'middleware' => ['web', 'lang', 'frontuser']], function () {
	
	Route::get(  '/my-account', 'Front\FrontUserController@myAccount' );
	Route::post( '/my-account', 'Front\FrontUserController@saveMyAccount' );
	
	Route::get(  '/edit-profile', 'Front\FrontUserController@editProfile' );
	Route::post( '/edit-profile', 'Front\FrontUserController@saveProfile' );
	
	Route::post( '/deletion-request', 'Front\FrontUserController@deletionRequest' );
	
	Route::post( '/change-password', 'Front\FrontUserController@changePassword' );
	
	Route::get( '/tips/{slug}', 'Front\TipsController@index' );
	
	
});

## Front Login / Register / Forgot Password / Reset Password / Logout
Route::group([ 'prefix' => '/', 'middleware' => ['web', 'lang']], function () {
	
	Route::get(	 '/register', 'Front\FrontUserController@showRegisterForm' );
	Route::post( '/register', 'Front\FrontUserController@register' );
	
	Route::get(	 '/login', 'Front\FrontUserController@showLoginForm' );
	Route::post( '/login', 'Front\FrontUserController@login' );
	
	Route::get(	 '/forgot-password', 'Front\FrontUserController@showForgotPasswordForm' );
	Route::post( '/forgot-password', 'Front\FrontUserController@forgotpassword' );
	
	Route::get(  '/reset-password/{reset_token}', 'Front\FrontUserController@showResetPasswordForm');
	Route::post( '/reset-password/{reset_token}', 'Front\FrontUserController@resetpassword');
	
	Route::get(  '/logout', 'Front\FrontUserController@logout' );
	
	Route::get(  '/tcsDemo', 'Front\FrontUserController@tcsDemo' );
	
	
});


## Admin Login / Register
Route::group([ 'prefix' => 'admin/', 'middleware' => ['web', 'lang']], function () {
	Route::get( '', function(){ 
		return redirect('admin/login'); 
	});
	Route::get( 'register', 'Admin\RegisterController@showForm' );
	Route::post( 'register', 'Admin\RegisterController@register' );
	Route::get( 'login', 'Admin\LoginController@showForm' );
	Route::post( 'login', 'Admin\LoginController@login' );
	Route::get( 'logout', 'Admin\LoginController@logout' );
});



## Admin Access
Route::group([ 'prefix' => 'admin/', 'middleware' => [ 'web', 'admin', 'lang' ] ], function(){
	
	## One Page Setting Routes
	$routeArr = array(
		'general-settings' 		=> 'Admin\GeneralSettingsController',
		'tcs-settings' 			=> 'Admin\TCSController',
		'custom-html-fields' 	=> 'Admin\CustomHtmlFieldsController',
		'email-settings' 		=> 'Admin\EmailSettingController',
		'sms-settings' 			=> 'Admin\SmsSettingController',
	);
	foreach( $routeArr as $k_route => $controller ){
		Route::get(  $k_route, $controller.'@index' );
		Route::post( $k_route, $controller.'@index' );	
	}
	
	## Ajax Controller
	Route::get( 'load-parent-faqs', 'Admin\AjaxController@loadParentFaq' );
	Route::get( 'load-tips-groups', 'Admin\AjaxController@loadTipsGroups' );
	
	## Add / Edit / Delete Routes
	$routeArr = array(
		'manage-admin' 			=> 'Admin\AdminController',
		'manage-ccusers' 		=> 'Admin\CcAgentController',
		'languages' 			=> 'Admin\LanguageController',
		'homepage-slider' 		=> 'Admin\HomepageSliderController',
		'email' 				=> 'Admin\EmailController',
		'sms' 					=> 'Admin\SmsController',
		'pages' 				=> 'Admin\PageController',
		'faq-types' 			=> 'Admin\FaqTypeController',
		'faqs' 					=> 'Admin\FaqController',
		
		'users' 				=> 'Admin\FrontUserController',
		'users-assets-results' 	=> 'Admin\AssetsResultsController',
		
		'manage-translation' 	=> 'Admin\TranslationController',
		'country' 				=> 'Admin\CountryController',
		'glossary' 				=> 'Admin\GlossaryController',
		'tips-types' 			=> 'Admin\TipsTypeController',
		'tips-groups' 			=> 'Admin\TipsGroupController',
		'tips' 					=> 'Admin\TipsController',
		'rss-feeds' 			=> 'Admin\AdminRssController',
	);
	
	foreach( $routeArr as $k_route => $controller ){
		Route::get(  $k_route, $controller.'@index' );
		Route::get(  $k_route.'/{action}', $controller.'@action' );
		Route::post( $k_route.'/{action}', $controller.'@action' );
		Route::get(  $k_route.'/{action}/{_id}', $controller.'@action' );
		Route::post( $k_route.'/{action}/{_id}', $controller.'@action' );
	}
	
});


## CC Agent Login / Register
Route::group([ 'prefix' => 'cc_admin/', 'middleware' => ['web', 'lang']], function () {
	Route::get( '', function(){ 
		return redirect('cc_admin/login'); 
	});
	Route::get( 'login', 'Admin\CcAgentLoginController@showForm' );
	Route::post( 'login', 'Admin\CcAgentLoginController@login' );
	Route::get( 'logout', 'Admin\CcAgentLoginController@logout' );
});

## CC Agent Access
Route::group([ 'prefix' => 'cc_admin/', 'middleware' => [ 'web', 'cc_admin', 'lang' ] ], function(){
	
	Route::get(  'users', 'Admin\FrontUserController@indexCcAgent' );
	Route::get(  'users/{action}/{_id}', 'Admin\FrontUserController@actionCcAgent' );
	Route::post( 'users/{action}/{_id}', 'Admin\FrontUserController@actionCcAgent' );
	
	Route::get( 'users-assets-results', 'Admin\AssetsResultsController@indexCcAgent' );
	Route::get( 'users-assets-results/{action}/{_id}', 'Admin\AssetsResultsController@actionCcAgent' );
	
});




## Basic API Calls
Route::get( 'api/get-label-translation', 'ApiController@getLabelTranslation' );

// Route::get('/phpinfo', function () { phpinfo(); exit; });

## Basic API Calls
Route::get( 'test/sms', 'Front\SmsController@test' );
// http://api.clickatell.com/http/sendmsg?user=philipp.hoyer.i-surance&password=i-surance_2017&api_id=3640644&to=919998120220&text=SafeonlineMessage

